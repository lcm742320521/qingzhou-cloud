package com.qingzhou.auth.service;

import cn.hutool.captcha.*;
import cn.hutool.captcha.generator.MathGenerator;
import cn.hutool.core.math.Calculator;
import cn.hutool.core.util.StrUtil;
import com.qingzhou.auth.config.CaptchaConfig;
import com.qingzhou.common.core.constants.CacheConstant;
import com.qingzhou.common.core.web.exception.ServiceException;
import com.qingzhou.common.redis.service.RedisService;
import com.qingzhou.system.api.RemoteSysConfigService;
import org.noear.nami.annotation.NamiClient;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.cloud.CloudClient;
import org.noear.solon.core.handle.Result;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

/**
 * 验证码处理
 * @author xm
 */
@Component
public class CaptchaService {

    // 图形验证码宽高
    private static final int CAPTCHA_WIDTH = 160;
    private static final int CAPTCHA_HEIGHT = 60;

    @Inject
    CaptchaConfig captchaConfig;

    @Inject
    RedisService redisService;

    @NamiClient
    RemoteSysConfigService remoteSysConfigService;

    /**
     * 获取验证码
     */
    public Result<Map<String, Object>> getCaptcha() {
        Map<String, Object> map = new HashMap<>();
        boolean captchaEnabled = captchaConfig.getCaptchaEnabled();
        map.put("captchaEnabled", captchaEnabled);
        // 是否开启注册用户
        Result<String> result = remoteSysConfigService.getSysConfigByKey("sys.account.registerUser");
        map.put("register", Result.SUCCEED_CODE == result.getCode() && "true".equals(result.getData()));
        if (captchaEnabled) {
            // 如果开启了验证码，生成验证码
            String captchaInvadeType = captchaConfig.getCaptchaInvadeType();
            // 干扰类型
            if ("circle".equals(captchaInvadeType)) {
                // 圆圈干扰验证码（宽、高、验证码字符数、干扰元素个数）
                CircleCaptcha circleCaptcha = CaptchaUtil.createCircleCaptcha(CAPTCHA_WIDTH, CAPTCHA_HEIGHT, 4, 10);
                createCaptcha(circleCaptcha, map);
            } else if("shear".equals(captchaInvadeType)) {
                // 扭曲干扰验证码（宽、高、验证码字符数、干扰线宽度）
                ShearCaptcha shearCaptcha = CaptchaUtil.createShearCaptcha(CAPTCHA_WIDTH, CAPTCHA_HEIGHT, 4, 5);
                createCaptcha(shearCaptcha, map);
            } else {
                // 线段干扰验证码（宽、高、验证码字符数、干扰元素个数）
                LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(CAPTCHA_WIDTH, CAPTCHA_HEIGHT, 4, 15);
                createCaptcha(lineCaptcha, map);
            }
        }
        return Result.succeed(map);
    }

    /**
     * 校验验证码
     */
    public void checkCapcha(String code, String uuid) {
        // 如果开启了验证码
        if(captchaConfig.getCaptchaEnabled()) {
            if(!StrUtil.isAllNotEmpty(code, uuid)) {
                throw new ServiceException("验证码必填");
            }
            String codeKey = CacheConstant.CAPTCHA_CODE_KEY + uuid;
            // 校验
            String cacheCode = redisService.getCacheObject(codeKey);
            if(StrUtil.isEmpty(cacheCode)) {
                throw new ServiceException("验证码已失效");
            }
            redisService.deleteCacheObject(codeKey);
            if(!StrUtil.equalsIgnoreCase(code, cacheCode)) {
                throw new ServiceException("验证码错误");
            }
        }
    }

    /**
     * 生成验证码
     * @param t
     * @param map
     * @param <T>
     */
    private <T extends AbstractCaptcha> void createCaptcha(T t, Map<String, Object> map) {
        String uuid = String.valueOf(CloudClient.id().generate());
        String codeKey = CacheConstant.CAPTCHA_CODE_KEY + uuid;
        // 验证码类型
        String captchaType = captchaConfig.getCaptchaType();
        // 验证码的内容（如果是字符串类型，则验证码的内容就是结果）
        String code = t.getCode();
        // 如果是四则运算
        if("math".equals(captchaType)) {
            t.setGenerator(new MathGenerator(1));
            // 重新生成code
            t.createCode();
            // 运算结果
            int result = (int) Calculator.conversion(t.getCode());
            code = String.valueOf(result);
        }
        String img = t.getImageBase64();

        // 将结果放入缓存
        redisService.setCacheObject(codeKey, code, Duration.ofMinutes(CacheConstant.CAPTCHA_CODE_EXPIRATION));

        map.put("uuid", uuid);
        map.put("img", img);
    }

}
