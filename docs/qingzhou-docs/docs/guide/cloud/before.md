---
title: 项目运行
prev: false
# next: ./dev
---

## 后端
::: tip 请确认您已有以下运行环境
1. JDK 1.8 及以上
2. Maven 版需要安装 Maven 3.6.3 及以上，Gradle 版无需安装 Maven 及 Gradle
:::

::: warning 另外您还需要安装以下中间件并启动
1. MySQL 5.6.x 及以上
2. Redis 3.x 及以上
3. Nacos 2.x
:::

### 1、Nacos
您需要向`Nacos`中导入所有微服务模块的配置文件。
* 首先在`Nacos`中创建命名空间：qingzhou
  ::: danger 特别注意
  您需要保证创建的`命名空间id`与后端项目每个微服务模块的配置文件`app.yml`中配置的`nacos.namespace`保持一致！
  :::
  ![namespace](../../.vuepress/public/cloud/namespace.png)
  * 您可以在创建命名空间时将`app.yml`中配置的`nacos.namespace`作为`命名空间id`填入
  * 您也可以在创建命名空间后，将`Nacos`自动生成的`命名空间id`配置到每个微服务模块的配置文件`app.yml`中的`nacos.namespace`
* 在创建的命名空间 qingzhou 中导入配置文件：
  ![namespace](../../.vuepress/public/cloud/nacos1.png)
  ![namespace](../../.vuepress/public/cloud/nacos2.png)  
  选择 sql 目录下的压缩包 qingzhou-cloud/sql/nacos_config_export_xxx.zip
* 导入成功：
  ![namespace](../../.vuepress/public/cloud/nacos3.png)

### 2、数据库
* 在`MySQL`中创建数据库：qingzhou_cloud
  ![mysql](../../.vuepress/public/cloud/mysql.png)
* 向数据库 qingzhou_boot 中导入表：qingzhou-cloud/sql/qingzhou_cloud.sql
* 修改`Nacos`中每个配置文件的数据库连接配置：
  ```yaml {6-8}
  # Redis配置
  redis:
    cache_source:
      config: |
        singleServerConfig:
          address: redis://127.0.0.1:6379
          password:
          database: 7
  ```
  > 请确保 Redis 指定库中没有缓存数据
  ```yaml {12-14}
  # 动态数据源
  solon:
    dataSources:
      data_source!:
        class: org.noear.solon.data.dynamicds.DynamicDataSource
        # 严格模式（指定的源不存时，严格模式会抛异常；非严格模式用默认源）
        strict: true
        # default 或 master ，会做为动态源的默认源
        master:
          dataSourceClassName: com.zaxxer.hikari.HikariDataSource
          driverClassName: com.mysql.cj.jdbc.Driver
          jdbcUrl: jdbc:mysql://127.0.0.1:3306/qingzhou_cloud?useUnicode=true&characterEncoding=utf8&autoReconnect=true&rewriteBatchedStatements=true
          username: root
          password:
  ```

### 3、运行后端
* 启动：
  * QingZhouAuthApplication *必要*
  * QingZhouSystemApplication *必要*
  * QingZhouGenApplication
  * QingZhouFileApplication
  * QingZhouScheduleApplication
  * QingZhouDemoApplication
  * QingZhouDocsApplication
::: warning 注意
如果是`Gradle`版，并且您的`IDEA`版本较旧（比如`2018.3`版），启动后访问系统可能会报错：
> Error querying database. Cause: org.apache.ibatis.binding.BindingException: Parameter 'userName' not found.

或者在账号密码确实输入正确，但依旧提示密码错误。  
这是由于`Gradle`版本与`IDEA`版本兼容性导致的编译参数`-parameters`没生效。  
您需要在`File -> Settings -> Build,Execution,Deployment -> Compiler -> Java Compiler`中的`Javac Options`下的`Additional command line parameters`中添加`-parameters`。

![gradle_settings](../../.vuepress/public/boot/gradle_settings.png)
添加后`clean`，重新编译。

`IDEA`推荐使用`2021.3`及以上版本。
:::

## 前端
::: tip 请确认您已有以下运行环境
1. Node.js 14.x 及以上
:::

### 1、运行前端
```bash
# 进入前端项目目录，在终端中运行命令
cd qingzhou-cloud-ui
# cd qingzhou-cloud-ui-vue3

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过指定镜像源解决 npm 下载速度慢的问题
# npm install --registry=https://registry.npmmirror.com

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80
