---
title: 简介（微服务版）
prev: false
# next: ./before
---

<el-alert
  title="当前为 QingZhou Cloud v3.x 版本的文档"
  description="v2.x 文档请前往 v2.5.1 版本下的 docs/qingzhou-docs"
  type="warning"
  effect="dark"
  show-icon>
</el-alert>

### 简介
[QingZhou Cloud](https://gitee.com/opensolon/qingzhou-cloud) 是基于 Solon + Vue 的前后端分离的快速开发脚手架，采用微服务架构，属于若依脚手架的 Solon 版本。

**来看下架构图**
![k8s](../../.vuepress/public/cloud/cloud.png)

### 内置功能
* 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
* 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
* 岗位管理：配置系统用户所属担任职务。
* 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
* 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
* 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
* 参数管理：对系统动态配置常用参数。
* 通知公告：系统通知公告信息发布维护。
* 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
* 登录日志：系统登录日志记录查询包含登录异常。
* 在线构建：拖动表单元素生成相应的HTML代码。
* 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载。
* 定时任务：任务调度。
* 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
* 系统接口：根据业务代码自动生成相关的api接口文档。
* 在线用户：当前系统中活跃用户状态监控。

### 技术选型
- 后端
  - Solon
  - Solon Cloud
  - HikariCP
  - Mybatis-Flex
  - Redis
  - Sa-Token
  - Hutool
  - ···
- 前端
  - Vue
  - Vue-Router
  - Vuex / Pinia
  - Element-UI / Element-Plus
  - Axios
  - Js-Cookie
  - ···

### 版本分支
* 默认`master`分支，后端采用`Maven`作为构建工具。
* `gradle`分支，后端采用`Gradle`作为构建工具。
> 不建议直接`clone`分支代码，推荐下载标签中的最新版本。

### 项目结构
~~~
qingzhou-cloud（Maven版）
├── docs                                  // 文档目录
│   ├── qingzhou-docs                     // 项目配套文档
├── qingzhou-api                          // 接口模块
│       └── qingzhou-api-system           // 系统接口
├── qingzhou-auth                         // 认证中心 [8101]
├── qingzhou-cloud-ui                     // 前端框架 [80]（Vue2）
├── qingzhou-cloud-ui-vue3                // 前端框架 [80]（Vue3）
├── qingzhou-common                       // 通用模块
│       └── qingzhou-common-core          // 核心模块
│       └── qingzhou-common-datasource    // 多数据源
│       └── qingzhou-common-log           // 日志记录
│       └── qingzhou-common-redis         // 缓存服务
│       └── qingzhou-common-security      // 安全模块
│       └── qingzhou-common-swagger       // 文档模块
├── qingzhou-modules                      // 业务模块
│       └── qingzhou-modules-demo         // 演示服务 [8103]
│       └── qingzhou-modules-docs         // 文档服务 [8095]
│       └── qingzhou-modules-file         // 文件服务 [8091]
│       └── qingzhou-modules-gen          // 代码生成 [8092]
│       └── qingzhou-modules-schedule     // 任务调度 [8093]
│       └── qingzhou-modules-system       // 系统服务 [8102]
├── qingzhou-visual                       // 图形化管理模块
│       └── qingzhou-visual-monitor       // 监控中心 [8094]
├── sql                                   // sql目录
│   ├── K8S                               // 项目配套K8S配置参考
│   ├── nacos_config_export_xxx.zip       // 项目配套服务配置文件（Nacos）
│   ├── nginx.conf                        // 项目配套Nginx配置参考文件
│   ├── qingzhou_boot.sql                 // 项目配套数据库文件
├── .gitignore                            // git 忽略项
├── LICENSE                               // 许可证
├── pom.xml                               // 公共依赖
├── README.md                             // README说明
~~~
~~~
com.qingzhou（Gradle版）
├── docs                                  // 文档目录
│   ├── qingzhou-docs                     // 项目配套文档
├── gradle                                // gradle包装器
├── qingzhou-api                          // 接口模块
│       └── qingzhou-api-system           // 系统接口
├── qingzhou-auth                         // 认证中心 [8101]
├── qingzhou-cloud-ui                     // 前端框架 [80]（Vue2）
├── qingzhou-cloud-ui-vue3                // 前端框架 [80]（Vue3）
├── qingzhou-common                       // 通用模块
│       └── qingzhou-common-core          // 核心模块
│       └── qingzhou-common-datasource    // 多数据源
│       └── qingzhou-common-log           // 日志记录
│       └── qingzhou-common-redis         // 缓存服务
│       └── qingzhou-common-security      // 安全模块
│       └── qingzhou-common-swagger       // 文档模块
├── qingzhou-modules                      // 业务模块
│       └── qingzhou-modules-demo         // 演示服务 [8103]
│       └── qingzhou-modules-docs         // 文档服务 [8095]
│       └── qingzhou-modules-file         // 文件服务 [8091]
│       └── qingzhou-modules-gen          // 代码生成 [8092]
│       └── qingzhou-modules-schedule     // 任务调度 [8093]
│       └── qingzhou-modules-system       // 系统服务 [8102]
├── qingzhou-visual                       // 图形化管理模块
│       └── qingzhou-visual-monitor       // 监控中心 [8094]
├── sql                                   // sql目录
│   ├── K8S                               // 项目配套K8S配置参考
│   ├── nacos_config_export_xxx.zip       // 项目配套服务配置文件（Nacos）
│   ├── nginx.conf                        // 项目配套Nginx配置参考文件
│   ├── qingzhou_boot.sql                 // 项目配套数据库文件
├── .gitignore                            // git 忽略项
├── build.gradle                          // gradle构建脚本
├── gradlew                               // Linux脚本
├── gradlew.bat                           // Windows脚本
├── LICENSE                               // 许可证
├── README.md                             // README说明
├── settings.gradle                       // gradle项目配置
~~~
~~~
qingzhou-cloud-ui 前端项目（vue2版）
├── build                                 // 构建相关
├── public                                // 公共文件
│   ├── favicon.ico                       // favicon图标
│   └── index.html                        // html模板
│   └── robots.txt                        // 反爬虫
├── src                                   // 源代码
│   ├── api                               // 所有请求（后台接口）
│   ├── assets                            // 图片 样式等静态资源
│   ├── components                        // 全局公用组件
│   ├── directive                         // 自定义指令
│   ├── layout                            // 布局
│   ├── plugins                           // 插件（功能增强：比如全局方法挂载、全局组件挂载等）
│   ├── router                            // 路由
│   ├── store                             // Vuex
│   ├── utils                             // 全局公用方法
│   ├── views                             // 所有页面
│   ├── App.vue                           // 入口页面
│   ├── main.js                           // 入口 初始化Vue
├── .editorconfig                         // EditorConfig插件配置
├── .env.development                      // 开发环境配置
├── .env.production                       // 生产环境配置
├── .eslintignore                         // 忽略语法检查
├── .eslintrc.js                          // eslint 配置项
├── .gitignore                            // git 忽略项
├── babel.config.js                       // babel.config.js
├── package-lock.json                     // 包版本锁定
├── package.json                          // 包管理
├── README.md                             // README说明
└── vue.config.js                         // vue.config.js
~~~
~~~
qingzhou-cloud-ui-vue3 前端项目（vue3版）
├── public                                // 公共文件
│   ├── favicon.ico                       // favicon图标
├── src                                   // 源代码
│   ├── api                               // 所有请求（后台接口）
│   ├── assets                            // 图片 样式等静态资源
│   ├── components                        // 全局公用组件
│   ├── directive                         // 自定义指令
│   ├── layout                            // 布局
│   ├── plugins                           // 插件（功能增强：比如全局方法挂载、全局组件挂载等）
│   ├── router                            // 路由
│   ├── store                             // Pinia
│   ├── utils                             // 全局公用方法
│   ├── views                             // 所有页面
│   ├── App.vue                           // 入口页面
│   ├── main.js                           // 入口 初始化Vue
├── .editorconfig                         // EditorConfig插件配置
├── .env.development                      // 开发环境配置
├── .env.production                       // 生产环境配置
├── .eslintignore                         // 忽略语法检查
├── .eslintrc-auto-import.json            // 忽略自动导入的语法检查
├── .eslintrc.js                          // eslint 配置项
├── .gitignore                            // git 忽略项
├── index.html                            // html模板
├── package-lock.json                     // 包版本锁定
├── package.json                          // 包管理
├── README.md                             // README说明
└── vite.config.js                        // vite.config.js
~~~
