---
title: 项目部署
prev: false
# next: ./v2
---

## 后端
### 1、打包
* Maven 版
  ```bash
  mvn clean package
  ```

  ::: tip 默认打 jar 包
  打包后会在 qingzhou-boot/qingzhou-admin/target 目录下生成 jar 包文件 qingzhou-admin.jar
  :::

* Gradle 版  
  双击：`Gradle插件/qingzhou-boot/Tasks/build`下的`clean`和`build`。

  ::: tip 默认打 jar 包
  打包后会在 qingzhou-boot/qingzhou-admin/build/libs 目录下生成 jar 包文件 qingzhou-admin.jar
  :::

### 2、运行
```bash
java –jar qingzhou-admin.jar
```

::: tip 支持添加启动参数以及加载外部配置文件
```bash
java –jar qingzhou-admin.jar --server.port=8081

java –jar qingzhou-admin.jar --solon.env=dev

java –jar qingzhou-admin.jar --solon.config.add=./app.yml
```
详见 [Solon 配置](https://solon.noear.org/article/482)
:::

### 3、jar包加密
如果您不希望 jar 包被反编译导致泄露源码，您可以对 jar 包进行加密（目前仅支持`Maven`版的`solon-maven-plugin`打包方式）。  
加密工具：[ClassFinal](https://gitee.com/lcm742320521/class-final)  
加密只需一步：添加加密插件
```xml
<!-- qingzhou-boot/qingzhou-admin/pom.xml -->
<build>
  <finalName>${project.artifactId}</finalName>
  <plugins>
    <plugin>
      <groupId>org.noear</groupId>
      <artifactId>solon-maven-plugin</artifactId>
    </plugin>

    <!-- jar包加密 -->
    <plugin>
      <groupId>com.gitee.lcm742320521</groupId>
      <artifactId>classfinal-maven-plugin</artifactId>
      <version>1.4.0</version>
      <configuration>
        <libjars>qingzhou-*.jar</libjars><!-- jar包lib下要加密jar文件名(可为空,多个用","分割) -->
        <packages>com.qingzhou</packages><!-- 加密的包名(可为空,多个用","分割。应包含所有需要加密的类的包名) -->
        <cfgfiles>*.yml</cfgfiles><!-- 需要加密的配置文件，一般是resources目录下的yml或properties文件(可为空,多个用","分割) -->
        <password>#</password><!-- 加密密码，如果是#号，则使用无密码模式加密 -->
      </configuration>
      <executions>
        <execution>
          <phase>package</phase>
          <goals>
            <goal>classFinal</goal>
          </goals>
        </execution>
      </executions>
    </plugin>
  </plugins>
</build>
```
添加以上打包加密插件后，正常打包即可（打包方式不变）。打包成功后可在 qingzhou-boot/qingzhou-admin/target 目录下看到两个 jar 包：
  * qingzhou-admin.jar
  * qingzhou-admin-encrypted.jar *加密后的*

请运行加密后的 jar 包，运行命令为：
```bash
java -javaagent:qingzhou-admin-encrypted.jar -jar qingzhou-admin-encrypted.jar
```

## 前端
### 1、打包
```bash
npm run build:prod
```

::: tip 提示
打包成功之后，会在根目录生成 dist 文件夹，通常是 .js 、.css、index.html 等静态文件。  
将 dist 文件夹发布到你的 Nginx 或者静态服务器即可，其中的 index.html 是后台服务的入口页面。
:::

### 2、Nginx 配置参考
~~~
worker_processes  1;

events {
  worker_connections  1024;
}

http {
  include            mime.types;
  default_type       application/octet-stream;
  sendfile           on;
  keepalive_timeout  65;

  server {
    listen       80;
    server_name  localhost;
    charset      utf-8;

    location / {
      root  /home/qingzhou/ui;
      try_files $uri $uri/ /index.html;
      index  index.html index.htm;
    }

    location /prod-api/ {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header REMOTE-HOST $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://localhost:8080/;
    }

    error_page  500 502 503 504  /50x.html;
    location = /50x.html {
      root  html;
    }
  }
}
~~~
