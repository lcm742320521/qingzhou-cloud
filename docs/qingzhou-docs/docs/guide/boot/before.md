---
title: 项目运行
prev: false
# next: ./dev
---

## 后端
::: tip 请确认您已有以下运行环境
1. JDK 1.8 及以上
2. Maven 版需要安装 Maven 3.6.3 及以上，Gradle 版无需安装 Maven 及 Gradle
:::

::: warning 另外您还需要安装以下中间件并启动
1. MySQL 5.6.x 及以上
2. Redis 3.x 及以上
:::

### 1、数据库
* 在 `MySQL` 中创建数据库：qingzhou_boot
  ![mysql](../../.vuepress/public/boot/mysql.png)
* 向数据库`qingzhou_boot`中导入表：qingzhou-boot/sql/qingzhou_boot.sql
* 修改项目中的数据库连接配置：qingzhou-boot/qingzhou-admin/src/main/resources/app-dev.yml
  ```yaml {6-8}
  # Redis配置
  redis:
    cache_source:
      config: |
        singleServerConfig:
          address: redis://127.0.0.1:6379
          password:
          database: 7
  ```
  > 请确保 Redis 指定库中没有缓存数据
  ```yaml {12-14}
  # 动态数据源
  solon:
    dataSources:
      data_source!:
        class: org.noear.solon.data.dynamicds.DynamicDataSource
        # 严格模式（指定的源不存时，严格模式会抛异常；非严格模式用默认源）
        strict: true
        # default 或 master ，会做为动态源的默认源
        master:
          dataSourceClassName: com.zaxxer.hikari.HikariDataSource
          driverClassName: com.mysql.cj.jdbc.Driver
          jdbcUrl: jdbc:mysql://127.0.0.1:3306/qingzhou_boot?useUnicode=true&characterEncoding=utf8&autoReconnect=true&rewriteBatchedStatements=true
          username: root
          password:
  ```

### 2、运行后端
* 使用`IDEA`打开`qingzhou-boot`，启动：
  * qingzhou-boot/qingzhou-admin/src/main/java/com/qingzhou/QingZhouBootApplication
::: warning 注意
如果是`Gradle`版，并且您的`IDEA`版本较旧（比如`2018.3`版），启动后访问系统可能会报错：
> Error querying database. Cause: org.apache.ibatis.binding.BindingException: Parameter 'userName' not found.

这是由于`Gradle`版本与`IDEA`版本兼容性导致的编译参数`-parameters`没生效。\
您需要在`File -> Settings -> Build,Execution,Deployment -> Compiler -> Java Compiler`中的`Javac Options`下的`Additional command line parameters`中添加`-parameters`。

![gradle_settings](../../.vuepress/public/boot/gradle_settings.png)
添加后`clean`，重新编译。

`IDEA`推荐使用`2021.3`及以上版本。
:::

## 前端
::: tip 请确认您已有以下运行环境
1. Node.js 14.x 及以上
:::

### 1、运行前端
```bash
# 进入前端项目目录，在终端中运行命令
cd qingzhou-boot-ui
# cd qingzhou-boot-ui-vue3

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过指定镜像源解决 npm 下载速度慢的问题
# npm install --registry=https://registry.npmmirror.com

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80
