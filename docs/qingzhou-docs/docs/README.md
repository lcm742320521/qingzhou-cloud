---
home: true
heroImage: /logo.png
heroText: QingZhou
tagline: 使用轻舟快速构建应用程序
actionText: 快速上手 →
actionLink: /guide/adumbrate/
features:
- title: 单体版
  details: 前后端分离的版本：前端技术有 Vue、ElementUi 等，后端技术有 Solon、Sa-Token、Mybatis-Flex 等。
- title: 微服务版
  details: 前后端分离的微服务架构版本：前端技术有 Vue、ElementUi 等，后端技术有 Solon Cloud、Nacos、Sa-Token、Mybatis-Flex 等。
- title: 移动端版
  details: 前后端分离的APP版本
footer: Copyright © 2023-2026 qingzhou All rights reserved.
---
