package com.qingzhou.common.security;

import com.qingzhou.common.security.config.JacksonConfig;
import com.qingzhou.common.security.config.SecurityConfig;
import com.qingzhou.common.security.config.XssConfig;
import com.qingzhou.common.security.filter.GlobalFilter;
import com.qingzhou.common.security.impl.NoRepeatSubmitCheckerImpl;
import com.qingzhou.common.security.impl.StpInterfaceImpl;
import com.qingzhou.common.security.interceptor.GlobalRouterInterceptor;
import org.noear.solon.core.AppContext;
import org.noear.solon.core.Plugin;

/**
 * 插件扩展
 * @author xm
 */
public class SecurityPlugin implements Plugin {

    /**
     * 注册Bean
     * @param context
     * @throws Throwable
     */
    @Override
    public void start(AppContext context) throws Throwable {
        // 生成Bean，并触发身上的注解处理（比如类上有 @Controller 注解；则会执行 @Controller 对应的处理）
        context.beanMake(JacksonConfig.class);

        context.beanMake(SecurityConfig.class);

        context.beanMake(XssConfig.class);

        context.beanMake(NoRepeatSubmitCheckerImpl.class);

        context.beanMake(StpInterfaceImpl.class);

        context.beanMake(GlobalFilter.class);

        context.beanMake(GlobalRouterInterceptor.class);
    }

}
