package com.qingzhou.common.security.config;

import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import org.noear.solon.serialization.jackson.JacksonActionExecutor;
import org.noear.solon.serialization.jackson.JacksonRenderFactory;

import java.text.SimpleDateFormat;

/**
 * Jackson配置
 * @author xm
 */
@Configuration
public class JacksonConfig {

    /**
     * 序列化
     * @param factory
     * @param executor
     */
    @Bean
    public void jsonInit(@Inject JacksonRenderFactory factory, @Inject JacksonActionExecutor executor) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        factory.config().setDateFormat(simpleDateFormat);
        executor.config().setDateFormat(simpleDateFormat);
    }

}
