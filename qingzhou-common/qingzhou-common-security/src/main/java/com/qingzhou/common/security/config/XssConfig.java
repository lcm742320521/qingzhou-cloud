package com.qingzhou.common.security.config;

import lombok.Data;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import java.util.List;

@Data
@Configuration
@Inject(value = "${qingzhou.xss}")
public class XssConfig {

    /**
     * 过滤开关
     */
    private Boolean enabled;

    /**
     * 排除路径
     */
    private List<String> excludes;

}
