package com.qingzhou.common.redis.config;

import com.qingzhou.common.redis.service.RedisService;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Condition;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import org.redisson.api.RedissonClient;
import org.redisson.solon.RedissonClientOriginalSupplier;

/**
 * Redis配置
 * @author xm
 */
@Configuration
public class RedisConfig {

    /**
     * RedissonClient
     * @param supplier
     * @return
     */
    @Bean
    public RedissonClient redissonClient(@Inject("${redis.cache_source}") RedissonClientOriginalSupplier supplier) {
        return supplier.get();
    }

    /**
     * RedisService
     * @return
     */
    @Bean
    @Condition(onClass = RedissonClient.class)
    public RedisService redisService(RedissonClient redissonClient) {
        return new RedisService(redissonClient);
    }

}
