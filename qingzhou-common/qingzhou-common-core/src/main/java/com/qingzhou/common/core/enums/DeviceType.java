package com.qingzhou.common.core.enums;

/**
 * 登录设备类型
 * @author xm
 */
public enum DeviceType {
    PC("00", "PC端"),
    APP("01", "APP端");

    private final String code;
    private final String info;

    DeviceType(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }

}
