package com.qingzhou.common.core.constants;

/**
 * 服务常量
 * @author xm
 */
public class ServiceConstant {

    /**
     * 服务名
     */
    // 认证服务
    public static final String SERVICE_AUTH = "qingzhou-auth";
    // 系统服务
    public static final String SERVICE_SYSTEM = "qingzhou-system";

    /**
     * 服务之间调用的请求路径
     */
    public static final String PATH_BASE = "/rpc";
    // 请求系统服务 - 日志
    public static final String PATH_TO_SYSTEM_LOG = PATH_BASE + "/system/sysLog";
    // 请求系统服务 - 用户
    public static final String PATH_TO_SYSTEM_USER = PATH_BASE + "/system/sysUser";
    // 请求系统服务 - 系统参数
    public static final String PATH_TO_SYSTEM_CONFIG = PATH_BASE + "/system/sysConfig";

}
