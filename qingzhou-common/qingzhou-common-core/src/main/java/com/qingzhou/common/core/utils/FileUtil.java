package com.qingzhou.common.core.utils;

import com.qingzhou.common.core.constants.HttpConstant;
import org.noear.solon.core.handle.Context;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 文件 工具类
 * @author xm
 */
public class FileUtil extends cn.hutool.core.io.FileUtil {

    /**
     * 下载
     * @param byteArrayOutputStream
     * @return
     */
    public static void download(ByteArrayOutputStream byteArrayOutputStream) {
        Context ctx = ContextUtil.getContext();
        ctx.contentType(HttpConstant.CONTENT_TYPE_FORM);
        try {
            byteArrayOutputStream.writeTo(ctx.outputStream());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
