package com.qingzhou.common.core.web.domain;

import com.mybatisflex.annotation.Column;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Entity基类（建表时必须包含以下除 dataScopeSql外的字段）
 * @author xm
 */
@Data
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 更新时间 */
    private Date updateTime;

    /** 删除标志（0代表存在 1代表删除） */
    private int delFlag;

    /** 备注 */
    private String remark;

    /** 数据权限sql拼接字段 */
    @Column(ignore = true)
    private String dataScopeSql;

}
