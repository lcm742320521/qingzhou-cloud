package com.qingzhou.common.core.web.nami;

import com.qingzhou.common.core.constants.HttpConstant;
import com.qingzhou.common.core.utils.ContextUtil;
import org.noear.nami.Filter;
import org.noear.nami.Invocation;
import org.noear.nami.Result;

/**
 * Nami过滤器
 */
public interface INamiClientFilter extends Filter {

    /**
     * 过滤器
     * @param inv
     * @return
     * @throws Throwable
     */
    default Result doFilter(Invocation inv) throws Throwable {
        // 通过过滤器添加请求头
        inv.headers.putAll(ContextUtil.getHeaders(HttpConstant.TOKEN_NAME));
        return inv.invoke();
    }

}
