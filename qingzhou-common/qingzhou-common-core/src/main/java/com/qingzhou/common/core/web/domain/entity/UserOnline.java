package com.qingzhou.common.core.web.domain.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 登录用户
 * @author xm
 */
@Data
public class UserOnline implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private Long userid;

    /**
     * 账号
     */
    private String userName;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 部门名称
     */
    private List<Device> devices;


    /**
     * 登录设备
     * @author xm
     */
    @Data
    public class Device {
        /**
         * 会话编号
         */
        private String token;

        /**
         * 设备
         */
        private String deviceType;

        /**
         * 登录IP地址
         */
        private String loginIp;

        /**
         * 登录时间（单位：秒）
         */
        private Long loginTime;
    }

}
