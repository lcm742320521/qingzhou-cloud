package com.qingzhou.common.core.utils;

import cn.hutool.core.util.StrUtil;
import com.qingzhou.common.core.constants.HttpConstant;
import org.noear.solon.core.handle.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * 请求上下文 工具类
 * @author xm
 */
public class ContextUtil {

    /**
     * 获取请求上下文
     * @return
     */
    public static Context getContext() {
        return Context.current();
    }

    /**
     * 获取客户端 IP
     * @return
     */
    public static String getIpAddr() {
        try {
            // 请求上下文
            Context ctx = getContext();
            return ctx.realIp();
        } catch (Exception e) {
            return HttpConstant.IP_UNKNOWN;
        }
    }

    /**
     * 获取请求头
     * @param headers
     * @return
     */
    public static Map<String, String> getHeaders(String... headers) {
        // 请求上下文
        Context ctx = getContext();
        Map<String, String> map = new HashMap<>();
        for (Map.Entry<String, String> stringStringEntry : ctx.headerMap().toValueMap().entrySet()) {
            // 鉴权请求头（Http协议规定了请求头大小写不敏感）
            if(StrUtil.equalsAnyIgnoreCase(stringStringEntry.getKey(), headers)) {
                map.put(stringStringEntry.getKey(), stringStringEntry.getValue());
            }
        }
        return map;
    }

}
