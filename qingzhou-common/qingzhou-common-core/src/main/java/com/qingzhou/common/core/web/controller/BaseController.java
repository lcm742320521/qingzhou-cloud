package com.qingzhou.common.core.web.controller;

import cn.hutool.core.convert.Convert;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mybatisflex.core.paginate.Page;
import com.qingzhou.common.core.web.domain.PageData;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.Valid;

import java.util.List;

/**
 * web层通用数据处理
 * @author xm
 */
@Valid
public class BaseController {

    /**
     * 设置请求分页数据
     * @param page
     */
    protected void startPage(Page<?> page) {
        Integer pageNumber = Convert.toInt(page.getPageNumber(), 1);
        Integer pageSize = Convert.toInt(page.getPageSize());
        PageHelper.startPage(pageNumber, pageSize);
    }

    /**
     * 清理分页的线程变量
     */
    protected void clearPage() {
        PageHelper.clearPage();
    }

    /**
     * 响应请求分页数据
     * @param list
     * @return
     */
    protected <T> PageData<T> getPageData(List<T> list) {
        return getPD(list, new PageInfo<>(list).getTotal());
    }

    /**
     * 响应请求分页数据
     * @param page
     * @return
     */
    protected <T> PageData<T> getPageData(Page<T> page) {
        return getPD(page.getRecords(), page.getTotalRow());
    }

    /**
     * 响应返回结果
     * @param result 结果
     * @return
     */
    protected Result<Void> toResult(boolean result) {
        return result ? Result.succeed() : Result.failure();
    }

    /**
     * 响应返回结果
     * @param rows 影响行数
     * @return
     */
    protected Result<Void> toResult(int rows) {
        return rows > 0 ? Result.succeed() : Result.failure();
    }

    /**
     * 分页数据
     * @param list
     * @param total
     * @return
     */
    private <T> PageData<T> getPD(List<T> list, long total) {
        PageData<T> pageData = new PageData<>();
        pageData.setList(list);
        pageData.setTotal(total);
        return pageData;
    }

}
