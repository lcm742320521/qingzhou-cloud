package com.qingzhou.common.core.constants;

/**
 * 缓存常量信息
 * @author xm
 */
public class CacheConstant {

    /**
     * 登录验证码
     */
    // redis key
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";
    //登录验证码有效期（分钟）
    public static final long CAPTCHA_CODE_EXPIRATION = 2L;

    /**
     * 登录失败
     */
    // redis key
    public static final String LOGIN_ERROR_KEY = "login_error:";

    /**
     * 登录用户
     */
    public static final String LOGIN_USER_KEY = "loginUser";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";

}
