package com.qingzhou.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 字典 枚举类
 * @author xm
 */
public class DictEnum {

    /**
     * 删除标识
     */
    @Getter
    @AllArgsConstructor
    public enum DEL_FLAG {
        OK(0, "正常"),
        DELETED(1, "删除");

        final int value;
        final String name;
    }

    /**
     * 通用状态
     */
    @Getter
    @AllArgsConstructor
    public enum COMMON_STATUS {
        OK("0", "正常"),
        DISABLE("1", "停用");

        final String value;
        final String name;
    }

    /**
     * 通用成功失败状态
     */
    @Getter
    @AllArgsConstructor
    public enum COMMON_SUCCESS_FAIL {
        SUCCESS("0", "成功"),
        FAIL("1", "失败");

        final String value;
        final String name;
    }

    /**
     * 通用是否
     */
    @Getter
    @AllArgsConstructor
    public enum COMMON_YES_NO {
        YES("Y", "是"),
        NO("N", "否");

        final String value;
        final String name;
    }

}
