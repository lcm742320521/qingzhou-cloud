package com.qingzhou.common.core.web.domain.model;

import com.qingzhou.common.core.web.domain.entity.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * 登录用户
 * @author xm
 */
@Data
public class LoginUser implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 用户ID
     */
    private Long userid;

    /**
     * 账号
     */
    private String username;

    /**
     * 登录设备类型
     */
    private String deviceType;

    /**
     * 登录时间（单位：秒）
     */
    private Long loginTime;

    /**
     * 过期时间（单位：秒）
     */
    private Long expireTime;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 用户信息
     */
    private SysUser sysUser;

    /**
     * 权限列表
     */
    private Set<String> permissions;

    /**
     * 角色列表
     */
    private Set<String> roles;

}
