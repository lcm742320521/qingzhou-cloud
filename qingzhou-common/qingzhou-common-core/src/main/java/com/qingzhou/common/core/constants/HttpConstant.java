package com.qingzhou.common.core.constants;

/**
 * 通用常量信息
 * @author xm
 */
public class HttpConstant {

    /**
     * 鉴权请求头（与 sa-token的 token-name保持一致）
     */
    public static final String TOKEN_NAME = "Authorization";

    /**
     * Http
     */
    public static final String HTTP = "http://";
    public static final String HTTPS = "https://";

    /**
     * 请求方式
     */
    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";
    public static final String HTTP_PUT = "PUT";
    public static final String HTTP_DELETE = "DELETE";

    /**
     * 状态码
     */
    public static final int HTTP_BAD_REQUEST = 400;
    public static final int HTTP_NOT_FOUND = 404;
    public static final int HTTP_INTERNAL_ERROR = 500;

    /**
     * www主域
     */
    public static final String WWW = "www.";

    /**
     * 未知 IP
     */
    public static final String IP_UNKNOWN = "unknown";

    /**
     * Content-Type
     */
    public static final String CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";

}
