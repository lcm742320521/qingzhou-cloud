package com.qingzhou.common.datasource.impl;

import com.mybatisflex.annotation.UpdateListener;
import com.qingzhou.common.core.web.domain.BaseEntity;
import com.qingzhou.common.security.utils.SecurityUtil;

import java.util.Date;

/**
 * 实体类更新 监听
 * @author xm
 */
public class DomainUpdateListenerImpl<T extends BaseEntity> implements UpdateListener {

    /**
     * 实体类执行更新时，自动赋值更新者和更新时间
     * @param entity
     */
    @SuppressWarnings("unchecked")
    @Override
    public void onUpdate(Object entity) {
        if(entity instanceof BaseEntity) {
            T t = (T) entity;
            t.setUpdateBy(SecurityUtil.getUserName());
            t.setUpdateTime(new Date());
        }
    }
}
