package com.qingzhou.common.datasource.config;

import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.audit.AuditManager;
import com.qingzhou.common.core.web.domain.BaseEntity;
import com.qingzhou.common.datasource.impl.DomainInsertListenerImpl;
import com.qingzhou.common.datasource.impl.DomainUpdateListenerImpl;
import org.noear.solon.annotation.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mybatis-Flex配置（在每个微服务的配置文件中还有全局配置）
 * @author xm
 */
@Configuration
public class MybatisFlexConfig {

    private static final Logger log = LoggerFactory.getLogger("mybatis-flex-sql");

    public MybatisFlexConfig() {
        globalConfig();
        sqlLog();
    }

    /**
     * 全局配置
     */
    private void globalConfig() {
        FlexGlobalConfig globalConfig = FlexGlobalConfig.getDefaultConfig();
        // 注册 insertListener
        globalConfig.registerInsertListener(new DomainInsertListenerImpl<>(), BaseEntity.class);
        // 注册 updateListener
        globalConfig.registerUpdateListener(new DomainUpdateListenerImpl<>(), BaseEntity.class);
    }

    /**
     * SQL打印
     */
    private void sqlLog() {
        // 开启审计功能（SQL打印分析的功能是使用 SQL审计模块完成的）
        AuditManager.setAuditEnable(true);

        // 设置 SQL审计收集器
        AuditManager.setMessageCollector(auditMessage ->
                log.info("SQL:【{}】, 耗时:【{}ms】", auditMessage.getFullSql(), auditMessage.getElapsedTime())
        );
    }

}
