package com.qingzhou.gen.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.gen.domain.GenTableColumn;

import java.util.List;

/**
 * 业务表字段 服务层
 * @author xm
 */
public interface IGenTableColumnService extends IService<GenTableColumn> {

    /**
     * 查询业务字段列表
     * @param tableId
     * @return
     */
    List<GenTableColumn> selectTableColumnListByTableId(Long tableId);

    /**
     * 根据表名查询表字段
     * @param tableName
     * @return
     */
    List<GenTableColumn> selectTableColumnsByName(String tableName);

    /**
     * 批量删除业务字段
     * @param tableIds
     * @return
     */
    int deleteGenTableColumnByIds(Long[] tableIds);

    /**
     * 删除业务字段
     * @param genTableColumns 列数据
     * @return
     */
    int deleteGenTableColumns(List<GenTableColumn> genTableColumns);

}
