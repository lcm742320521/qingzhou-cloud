package com.qingzhou.gen.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.gen.domain.GenTableColumn;

import java.util.List;

/**
 * 业务表字段 数据层
 * @author xm
 */
public interface GenTableColumnMapper extends BaseMapper<GenTableColumn> {

    /**
     * 查询业务字段列表
     * @param tableId
     * @param delFlag
     * @return
     */
    List<GenTableColumn> selectTableColumnListByTableId(Long tableId, int delFlag);

    /**
     * 根据表名查询表字段
     * @param tableName
     * @return
     */
    List<GenTableColumn> selectTableColumnsByName(String tableName);

    /**
     * 批量删除业务字段
     * @param tableIds
     * @return
     */
    int deleteGenTableColumnByIds(Long[] tableIds);

    /**
     * 删除业务字段
     * @param genTableColumns
     * @return
     */
    int deleteGenTableColumns(List<GenTableColumn> genTableColumns);

}
