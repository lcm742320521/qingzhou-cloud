package com.qingzhou.gen;

import org.noear.solon.Solon;
import org.noear.solon.admin.client.annotation.EnableAdminClient;
import org.noear.solon.annotation.SolonMain;

/**
 * 代码生成模块
 * @author xm
 */
//@EnableAdminClient
@SolonMain
public class QingZhouGenApplication {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Solon.start(QingZhouGenApplication.class, args);
        long times = System.currentTimeMillis() - start;
        System.out.println("(♥◠‿◠)ﾉﾞ  代码生成模块启动成功，耗时:【" + times + "ms】  ლ(´ڡ`ლ)ﾞ");
    }

}
