package com.qingzhou.gen.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.gen.domain.GenTable;

import java.util.List;

/**
 * 业务表 数据层
 * @author xm
 */
public interface GenTableMapper extends BaseMapper<GenTable> {

    /**
     * 查询数据库列表
     * @param ignoreTablePrefix
     * @param genTable
     * @return
     */
    List<GenTable> selectTables(List<String> ignoreTablePrefix, GenTable genTable);

    /**
     * 查询数据表
     * @param tableNames
     * @return
     */
    List<GenTable> selectTablesByNames(String[] tableNames);

    /**
     * 查询所有表信息
     * @return
     */
    List<GenTable> selectGenTableAll(int delFlag);

    /**
     * 查询表ID业务信息
     * @param tableId
     * @param delFlag
     * @return
     */
    GenTable selectGenTableById(Long tableId, int delFlag);

    /**
     * 查询表名称业务信息
     * @param tableName 表名称
     * @param delFlag
     * @return
     */
    GenTable selectGenTableByName(String tableName, int delFlag);

    /**
     * 批量删除业务
     * @param ids 需要删除的数据ID
     * @return
     */
    int deleteGenTableByIds(Long[] ids);

}
