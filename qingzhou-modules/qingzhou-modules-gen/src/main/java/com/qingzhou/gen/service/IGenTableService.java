package com.qingzhou.gen.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.gen.domain.GenTable;

import java.util.List;
import java.util.Map;

/**
 * 业务表 服务层
 * @author xm
 */
public interface IGenTableService extends IService<GenTable> {

    /**
     * 查询数据库列表
     * @param genTable
     * @return
     */
    List<GenTable> tables(GenTable genTable);

    /**
     * 查询代码生成详情
     * @param tableId
     * @return
     */
    Map<String, Object> info(Long tableId);

    /**
     * 预览代码生成
     * @param tableId
     * @return
     */
    Map<String, String> preview(Long tableId);

    /**
     * 新增代码生成（导入表）
     * @param tables
     */
    void add(String tables);

    /**
     * 修改代码生成
     * @param genTable
     * @return
     */
    int edit(GenTable genTable);

    /**
     * 删除代码生成
     * @param tableIds
     */
    void delete(Long[] tableIds);

    /**
     * 同步数据库
     * @param tableName
     */
    void synchDb(String tableName);

    /**
     * 生成代码（自定义路径）
     * @param tableName
     */
    void genCode(String tableName);

    /**
     * 批量生成代码
     * @param tables
     * @return
     */
    void batchGenCode(String tables);

}
