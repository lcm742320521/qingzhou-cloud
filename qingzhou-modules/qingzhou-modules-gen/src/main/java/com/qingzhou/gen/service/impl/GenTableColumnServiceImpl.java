package com.qingzhou.gen.service.impl;

import com.mybatisflex.solon.service.impl.ServiceImpl;
import com.qingzhou.common.core.enums.DictEnum;
import com.qingzhou.gen.domain.GenTableColumn;
import com.qingzhou.gen.mapper.GenTableColumnMapper;
import com.qingzhou.gen.service.IGenTableColumnService;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.annotation.Component;

import java.util.List;

/**
 * 业务表字段 服务层实现
 * @author xm
 */
@Component
public class GenTableColumnServiceImpl extends ServiceImpl<GenTableColumnMapper, GenTableColumn> implements IGenTableColumnService {

    @Db
    GenTableColumnMapper genTableColumnMapper;

    /**
     * 查询业务字段列表
     * @param tableId
     * @return
     */
    @Override
    public List<GenTableColumn> selectTableColumnListByTableId(Long tableId) {
        return genTableColumnMapper.selectTableColumnListByTableId(tableId, DictEnum.DEL_FLAG.OK.getValue());
    }

    /**
     * 根据表名查询表字段
     * @param tableName
     * @return
     */
    @Override
    public List<GenTableColumn> selectTableColumnsByName(String tableName) {
        return genTableColumnMapper.selectTableColumnsByName(tableName);
    }

    /**
     * 批量删除业务字段
     * @param tableIds
     * @return
     */
    @Override
    public int deleteGenTableColumnByIds(Long[] tableIds) {
        return genTableColumnMapper.deleteGenTableColumnByIds(tableIds);
    }

    /**
     * 删除业务字段
     * @param genTableColumns 列数据
     * @return
     */
    @Override
    public int deleteGenTableColumns(List<GenTableColumn> genTableColumns) {
        return genTableColumnMapper.deleteGenTableColumns(genTableColumns);
    }

}
