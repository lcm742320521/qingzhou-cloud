package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.domain.SysDictType;

/**
 * 数据字典类型 数据层
 * @author xm
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}
