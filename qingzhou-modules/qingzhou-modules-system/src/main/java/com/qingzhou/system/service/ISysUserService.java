package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.common.core.web.domain.entity.SysRole;
import com.qingzhou.common.core.web.domain.entity.SysUser;

import java.util.List;
import java.util.Map;

/**
 * 系统用户 服务层
 * @author xm
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 根据用户名查询用户
     * @param userName
     * @return
     */
    SysUser selectSysUserByUserName(String userName);

    /**
     * 查询用户列表
     * @param sysUser
     * @return
     */
    List<SysUser> selectUserList(SysUser sysUser);

    /**
     * 获取用户详情
     * @param userId
     * @param sysUser
     * @param sysRole
     * @return
     */
    Map<String, Object> info(Long userId, SysUser sysUser, SysRole sysRole);

    /**
     * 新增用户
     * @param sysUser
     * @return
     */
    int add(SysUser sysUser);

    /**
     * 注册用户
     * @param sysUser 用户信息
     * @return
     */
    int registerUser(SysUser sysUser);

    /**
     * 修改用户
     * @param sysUser
     * @return
     */
    int edit(SysUser sysUser);

    /**
     * 删除用户
     * @param userIds
     * @param sysUser
     * @return
     */
    int delete(Long[] userIds, SysUser sysUser);

    /**
     * 重置密码
     * @param sysUser
     */
    int resetPwd(SysUser sysUser);

    /**
     * 状态修改
     * @param sysUser
     * @return
     */
    int changeStatus(SysUser sysUser);

    /**
     * 根据用户编号获取授权角色
     * @param userId
     * @param sysRole
     * @return
     */
    Map<String, Object> authRole(Long userId, SysRole sysRole);

    /**
     * 用户授权角色
     * @param userId
     * @param roleIds
     * @param sysUser
     */
    void insertAuthRole(Long userId, Long[] roleIds, SysUser sysUser);

    /**
     * 查询角色已授权用户列表
     * @param sysUser 用户信息
     * @return
     */
    List<SysUser> selectAllocatedList(SysUser sysUser);

    /**
     * 查询角色未授权用户列表
     * @param user 用户信息
     * @return
     */
    List<SysUser> selectUnallocatedList(SysUser user);

    /**
     * 校验账号是否唯一
     * @param sysUser 用户信息
     * @return
     */
    boolean checkUserNameUnique(SysUser sysUser);

    /**
     * 个人中心 - 个人信息
     * @return
     */
    Map<String, Object> profile();

    /**
     * 个人中心 - 修改用户
     * @param sysUser
     * @return
     */
    int updateProfile(SysUser sysUser);

    /**
     * 个人中心 - 修改密码
     * @param oldPassword
     * @param newPassword
     * @return
     */
    int updateProfilePwd(String oldPassword, String newPassword);

    /**
     * 个人中心 - 修改头像
     * @param avatar
     * @return
     */
    int updateProfileAvatar(String avatar);

}
