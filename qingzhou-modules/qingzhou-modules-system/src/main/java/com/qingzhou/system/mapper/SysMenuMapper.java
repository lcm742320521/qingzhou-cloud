package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.domain.SysMenu;

import java.util.List;

/**
 * 系统菜单 数据层
 * @author xm
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 查询系统菜单列表
     * @param sysMenu 菜单信息
     * @param delFlag
     * @return
     */
    List<SysMenu> selectMenuList(SysMenu sysMenu, int delFlag);

    /**
     * 根据用户查询系统菜单列表
     * @param sysMenu 菜单信息
     * @param userId
     * @param delFlag
     * @return
     */
    List<SysMenu> selectMenuListByUserId(SysMenu sysMenu, Long userId, int delFlag);

    /**
     * 查询所有菜单树信息
     * @param menuTypes
     * @param status
     * @param delFlag
     * @return
     */
    List<SysMenu> selectMenuTreeAll(String[] menuTypes, String status, int delFlag);

    /**
     * 根据用户 ID查询菜单树信息
     * @param menuTypes
     * @param userId
     * @param status
     * @return
     */
    List<SysMenu> selectMenuTreeByUserId(String[] menuTypes, Long userId, String status, int delFlag);

    /**
     * 根据角色ID查询权限
     * @param roleId 角色ID
     * @param status
     * @param delFlag
     * @return
     */
    List<String> selectMenuPermsByRoleId(Long roleId, String status, int delFlag);

    /**
     * 根据角色ID查询菜单树信息
     * @param roleId 角色ID
     * @param menuCheckStrictly 菜单树选择项是否关联显示
     * @param delFlag
     * @return
     */
    List<Long> selectMenuListByRoleId(Long roleId, boolean menuCheckStrictly, int delFlag);

    /**
     * 根据用户ID查询权限
     * @param userId 用户ID
     * @param status
     * @param delFlag
     * @return
     */
    List<String> selectMenuPermsByUserId(Long userId, String status, int delFlag);

}
