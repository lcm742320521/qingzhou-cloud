package com.qingzhou.system.rpc;

import cn.hutool.core.util.ObjectUtil;
import com.qingzhou.common.core.constants.ServiceConstant;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.system.api.RemoteSysUserService;
import com.qingzhou.common.core.web.domain.model.LoginUser;
import com.qingzhou.common.core.web.domain.entity.SysUser;
import com.qingzhou.system.service.ISysMenuService;
import com.qingzhou.system.service.ISysRoleService;
import com.qingzhou.system.service.ISysUserService;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Remoting;
import org.noear.solon.core.handle.Result;

import java.util.Set;

/**
 * 远程调用系统服务实现 - 用户
 * @author xm
 */
@Remoting
@Mapping(ServiceConstant.PATH_TO_SYSTEM_USER)
public class RemoteSysUserServiceImpl extends BaseController implements RemoteSysUserService {

    @Inject
    ISysUserService sysUserService;

    @Inject
    ISysRoleService sysRoleService;

    @Inject
    ISysMenuService sysMenuService;

    /**
     * 根据用户名查询用户
     * @param userName
     */
    @Override
    public Result<LoginUser> getSysUserByUserName(String userName) {
        SysUser sysUser = sysUserService.selectSysUserByUserName(userName);
        if(ObjectUtil.isNull(sysUser)) {
            return Result.failure("用户名错误");
        }
        // 角色集合
        Set<String> roles = sysRoleService.getRolePermission(sysUser.getUserId());
        // 权限集合
        Set<String> permissions = sysMenuService.getMenuPermission(sysUser);

        LoginUser loginUser = new LoginUser();
        loginUser.setSysUser(sysUser);
        loginUser.setRoles(roles);
        loginUser.setPermissions(permissions);
        return Result.succeed(loginUser);
    }

    /**
     * 校验账号是否唯一
     * @param sysUser
     * @return
     */
    @Override
    public Result<Boolean> checkUserNameUnique(SysUser sysUser) {
        return Result.succeed(sysUserService.checkUserNameUnique(sysUser));
    }

    /**
     * 注册用户
     * @param sysUser
     * @return
     */
    @Override
    public Result<Integer> registerUser(SysUser sysUser) {
        return Result.succeed(sysUserService.registerUser(sysUser));
    }

}
