package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.common.core.web.domain.entity.SysUser;

import java.util.List;

/**
 * 系统用户 数据层
 * @author xm
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 根据用户名查询用户
     * @param userName
     * @param delFlag
     * @return
     */
    SysUser selectSysUserByUserName(String userName, int delFlag);

    /**
     * 通过用户ID查询用户
     * @param userId 用户ID
     * @return
     */
    SysUser selectUserById(Long userId);

    /**
     * 查询用户列表
     * @param sysUser
     * @param delFlag
     * @return
     */
    List<SysUser> selectUserList(SysUser sysUser, int delFlag);

    /**
     * 查询角色已授权用户列表
     * @param sysUser 用户信息
     * @param delFlag
     * @return
     */
    List<SysUser> selectAllocatedList(SysUser sysUser, int delFlag);

    /**
     * 查询角色未授权用户列表
     * @param sysUser 用户信息
     * @return
     */
    List<SysUser> selectUnallocatedList(SysUser sysUser, int delFlag);

}
