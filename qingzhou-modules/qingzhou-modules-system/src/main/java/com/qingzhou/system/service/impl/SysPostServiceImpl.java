package com.qingzhou.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import com.qingzhou.common.core.enums.DictEnum;
import com.qingzhou.common.core.web.exception.ServiceException;
import com.qingzhou.system.domain.SysPost;
import com.qingzhou.system.mapper.SysPostMapper;
import com.qingzhou.system.mapper.SysUserPostMapper;
import com.qingzhou.system.service.ISysPostService;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.annotation.Component;

import java.util.List;

/**
 * 岗位 服务层实现
 * @author xm
 */
@Component
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost> implements ISysPostService {

    @Db
    SysPostMapper sysPostMapper;

    @Db
    SysUserPostMapper sysUserPostMapper;

    /**
     * 新增岗位
     * @param sysPost
     * @return
     */
    @Override
    public int add(SysPost sysPost) {
        if (!checkPostNameUnique(sysPost)) {
            throw new ServiceException("岗位名称已存在");
        } else if(!checkPostCodeUnique(sysPost)) {
            throw new ServiceException("岗位编码已存在");
        }
        return sysPostMapper.insert(sysPost);
    }

    /**
     * 修改岗位
     * @param sysPost
     * @return
     */
    @Override
    public int edit(SysPost sysPost) {
        if (!checkPostNameUnique(sysPost)) {
            throw new ServiceException("岗位名称已存在");
        } else if(!checkPostCodeUnique(sysPost)) {
            throw new ServiceException("岗位编码已存在");
        }
        return sysPostMapper.update(sysPost);
    }

    /**
     * 删除岗位
     * @param postIds
     * @return
     */
    @Override
    public void delete(List<Long> postIds) {
        List<SysPost> sysPosts = sysPostMapper.selectListByIds(postIds);
        for (SysPost sysPost : sysPosts) {
            if(sysUserPostMapper.countUserPostByPostId(sysPost.getPostId()) > 0) {
                throw new ServiceException("岗位已分配,不能删除");
            }
            sysPostMapper.deleteById(sysPost.getPostId());
        }
    }

    /**
     * 根据用户ID获取岗位选择框列表
     * @param userId 用户ID
     * @return
     */
    @Override
    public List<Long> selectPostListByUserId(Long userId) {
        return sysPostMapper.selectPostListByUserId(userId, DictEnum.DEL_FLAG.OK.getValue());
    }

    /**
     * 查询用户所属岗位组
     * @param userName 用户名
     * @return
     */
    @Override
    public List<SysPost> selectPostsByUserName(String userName) {
        return sysPostMapper.selectPostsByUserName(userName, DictEnum.DEL_FLAG.OK.getValue());
    }

    /**
     * 校验岗位名称是否唯一
     * @param sysPost
     * @return
     */
    private boolean checkPostNameUnique(SysPost sysPost) {
        long postId = sysPost.getPostId() == null ? -1L : sysPost.getPostId();
        QueryWrapper qw = QueryWrapper.create();
        qw.and(SysPost::getPostName).eq(sysPost.getPostName());
        qw.limit(1);
        SysPost sp = sysPostMapper.selectOneByQuery(qw);
        return ObjectUtil.isNull(sp) || sp.getPostId() == postId;
    }

    /**
     * 校验岗位编码是否唯一
     * @param sysPost
     * @return
     */
    private boolean checkPostCodeUnique(SysPost sysPost) {
        long postId = sysPost.getPostId() == null ? -1L : sysPost.getPostId();
        QueryWrapper qw = QueryWrapper.create();
        qw.and(SysPost::getPostCode).eq(sysPost.getPostCode());
        qw.limit(1);
        SysPost sp = sysPostMapper.selectOneByQuery(qw);
        return ObjectUtil.isNull(sp) || sp.getPostId() == postId;
    }

}
