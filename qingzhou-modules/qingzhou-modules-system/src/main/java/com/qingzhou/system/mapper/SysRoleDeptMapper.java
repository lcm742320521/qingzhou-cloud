package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.domain.SysRoleDept;

/**
 * 角色与部门关联 数据层c
 * @author xm
 */
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

    /**
     * 通过角色ID删除角色和部门关联
     * @param roleId 角色ID
     * @return
     */
    int deleteRoleDeptByRoleId(Long roleId);

    /**
     * 批量删除角色部门关联信息
     * @param ids 需要删除的数据ID
     * @return
     */
    int deleteRoleDept(Long[] ids);

}
