package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.domain.SysUserRole;

/**
 * 用户与角色关联 数据层
 * @author xm
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    /**
     * 通过角色ID查询角色使用数量
     * @param roleId 角色ID
     * @return
     */
    int countUserRoleByRoleId(Long roleId);

    /**
     * 删除用户和角色关联信息
     * @param sysUserRole 用户和角色关联信息
     * @return
     */
    int deleteUserRoleInfo(SysUserRole sysUserRole);

    /**
     * 通过用户ID删除用户和角色关联
     * @param userId 用户ID
     * @return
     */
    int deleteUserRoleByUserId(Long userId);

    /**
     * 批量删除用户和角色关联
     * @param userIds 需要删除的用户ID
     * @return
     */
    int deleteUserRole(Long[] userIds);

    /**
     * 批量取消授权用户角色
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return
     */
    int deleteUserRoleInfos(Long roleId, Long[] userIds);

}
