package com.qingzhou.system;

import org.noear.solon.Solon;
import org.noear.solon.admin.client.annotation.EnableAdminClient;
import org.noear.solon.annotation.SolonMain;

/**
 * 系统模块
 * @author xm
 */
//@EnableAdminClient
@SolonMain
public class QingZhouSystemApplication {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Solon.start(QingZhouSystemApplication.class, args);
        long times = System.currentTimeMillis() - start;
        System.out.println("(♥◠‿◠)ﾉﾞ  系统模块启动成功，耗时:【" + times + "ms】  ლ(´ڡ`ლ)ﾞ");
    }

}
