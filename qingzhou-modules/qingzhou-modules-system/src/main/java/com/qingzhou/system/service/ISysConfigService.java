package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.system.domain.SysConfig;

import java.util.List;

/**
 * 系统参数配置 服务层
 * @author xm
 */
public interface ISysConfigService extends IService<SysConfig> {

    /**
     * 根据键名查询参数配置信息
     * @param configKey 参数键名
     * @return
     */
    String selectConfigByKey(String configKey);

    /**
     * 新增系统参数配置
     * @param sysConfig
     * @return
     */
    int add(SysConfig sysConfig);

    /**
     * 修改系统参数配置
     * @param sysConfig
     * @return
     */
    int edit(SysConfig sysConfig);

    /**
     * 删除系统参数配置
     * @param configIds
     * @return
     */
    void delete(List<Long> configIds);

    /**
     * 刷新系统参数配置缓存
     */
    void loadingConfigCache();

}
