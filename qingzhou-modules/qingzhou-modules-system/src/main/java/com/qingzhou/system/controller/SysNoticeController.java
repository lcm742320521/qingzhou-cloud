package com.qingzhou.system.controller;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.core.web.domain.PageData;
import com.qingzhou.system.domain.SysNotice;
import com.qingzhou.system.service.ISysNoticeService;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.Validated;

import java.util.Arrays;

/**
 * 通知公告 操作处理
 * @author xm
 */
@Controller
@Mapping("notice")
public class SysNoticeController extends BaseController {

    @Inject
    ISysNoticeService sysNoticeService;

    /**
     * 查询通知公告列表
     */
    @AuthPermissions("system:notice:list")
    @Get
    @Mapping("list")
    public PageData<SysNotice> list(Page<SysNotice> page, SysNotice sysNotice) {
        QueryWrapper qw = QueryWrapper.create();
        qw.and(SysNotice::getNoticeTitle).like(sysNotice.getNoticeTitle());
        qw.and(SysNotice::getNoticeType).eq(sysNotice.getNoticeType());
        qw.orderBy(SysNotice::getCreateTime).desc();
        Page<SysNotice> result = sysNoticeService.page(page, qw);
        return getPageData(result);
    }

    /**
     * 查询通知公告详情
     */
    @AuthPermissions("system:notice:query")
    @Get
    @Mapping("{id}")
    public SysNotice info(@Path Long id) {
        return sysNoticeService.getById(id);
    }

    /**
     * 新增通知公告
     */
    @QzLog(title = "通知公告", businessType = BusinessType.INSERT)
    @AuthPermissions("system:notice:add")
    @Post
    @Mapping
    public Result<Void> add(@Body @Validated SysNotice sysNotice) {
        return toResult(sysNoticeService.save(sysNotice));
    }

    /**
     * 修改通知公告
     */
    @QzLog(title = "通知公告", businessType = BusinessType.UPDATE)
    @AuthPermissions("system:notice:edit")
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated SysNotice sysNotice) {
        return toResult(sysNoticeService.updateById(sysNotice));
    }

    /**
     * 删除通知公告
     */
    @QzLog(title = "通知公告", businessType = BusinessType.DELETE)
    @AuthPermissions("system:notice:remove")
    @Delete
    @Mapping("{noticeIds}")
    public Result<Void> delete(@Path Long[] noticeIds) {
        return toResult(sysNoticeService.removeByIds(Arrays.asList(noticeIds)));
    }

}
