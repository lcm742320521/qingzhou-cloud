package com.qingzhou.system.domain;

import lombok.Data;

/**
 * 用户和角色关联 对象
 * @author xm
 */
@Data
public class SysUserRole {

    /** 用户ID */
    private Long userId;
    
    /** 角色ID */
    private Long roleId;

}
