package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.system.domain.SysDictType;

import java.util.List;

/**
 * 数据字典类型 服务层
 * @author xm
 */
public interface ISysDictTypeService extends IService<SysDictType> {

    /**
     * 新增数据字典类型
     * @param sysDictType
     * @return
     */
    int add(SysDictType sysDictType);

    /**
     * 修改数据字典类型
     * @param sysDictType
     * @return
     */
    int edit(SysDictType sysDictType);

    /**
     * 删除数据字典类型
     * @param dictIds
     * @return
     */
    void delete(List<Long> dictIds);

}
