package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.system.api.domain.SysOperLog;

/**
 * 操作日志 服务层
 * @author xm
 */
public interface ISysOperLogService extends IService<SysOperLog> {

    /**
     * 清空操作日志
     */
    void clean();

}
