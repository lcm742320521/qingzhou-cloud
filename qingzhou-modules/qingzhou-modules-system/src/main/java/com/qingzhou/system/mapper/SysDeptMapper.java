package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.common.core.web.domain.entity.SysDept;

import java.util.List;

/**
 * 部门 数据层
 * @author xm
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

    /**
     * 获取部门列表
     * @param sysDept
     * @param delFlag
     * @return
     */
    List<SysDept> selectDeptList(SysDept sysDept, int delFlag);

    /**
     * 根据角色ID查询部门树信息
     * @param roleId 角色ID
     * @param deptCheckStrictly 部门树选择项是否关联显示
     * @param delFlag
     * @return
     */
    List<Long> selectDeptListByRoleId(Long roleId, boolean deptCheckStrictly, int delFlag);

    /**
     * 根据ID查询所有子部门
     * @param deptId 部门ID
     * @return
     */
    List<SysDept> selectChildrenDeptById(Long deptId);

    /**
     * 根据ID统计所有子部门（正常状态）
     * @param deptId 部门ID
     * @param status
     * @param delFlag
     * @return
     */
    int selectNormalChildrenDeptById(Long deptId, String status, int delFlag);

    /**
     * 修改子元素关系
     * @param depts 子元素
     * @return
     */
    int updateDeptChildren(List<SysDept> depts);

    /**
     * 修改所在部门正常状态
     * @param deptIds 部门ID组
     * @param status
     */
    void updateDeptStatusNormal(Long[] deptIds, String status);

}
