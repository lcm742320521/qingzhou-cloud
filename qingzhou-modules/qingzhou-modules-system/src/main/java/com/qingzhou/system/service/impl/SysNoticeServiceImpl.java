package com.qingzhou.system.service.impl;

import com.mybatisflex.solon.service.impl.ServiceImpl;
import com.qingzhou.system.domain.SysNotice;
import com.qingzhou.system.mapper.SysNoticeMapper;
import com.qingzhou.system.service.ISysNoticeService;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.annotation.Component;

/**
 * 通知公告 服务层实现
 * @author xm
 */
@Component
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, SysNotice> implements ISysNoticeService {

    @Db
    SysNoticeMapper sysNoticeMapper;

}
