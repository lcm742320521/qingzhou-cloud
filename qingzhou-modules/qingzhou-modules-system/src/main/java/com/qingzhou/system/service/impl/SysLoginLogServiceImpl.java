package com.qingzhou.system.service.impl;

import com.mybatisflex.solon.service.impl.ServiceImpl;
import com.qingzhou.common.core.constants.CacheConstant;
import com.qingzhou.common.redis.service.RedisService;
import com.qingzhou.system.api.domain.SysLoginLog;
import com.qingzhou.system.mapper.SysLoginLogMapper;
import com.qingzhou.system.service.ISysLoginLogService;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

/**
 * 登录日志 服务层实现
 * @author xm
 */
@Component
public class SysLoginLogServiceImpl extends ServiceImpl<SysLoginLogMapper, SysLoginLog> implements ISysLoginLogService {

    @Db
    SysLoginLogMapper sysLoginLogMapper;

    @Inject
    RedisService redisService;

    /**
     * 解锁用户登录状态
     * @param userName
     * @return
     */
    @Override
    public void unlock(String userName) {
        redisService.deleteCacheObject(CacheConstant.LOGIN_ERROR_KEY + userName);
    }

    /**
     * 清空登录日志
     */
    @Override
    public void clean() {
        sysLoginLogMapper.clean();
    }

}
