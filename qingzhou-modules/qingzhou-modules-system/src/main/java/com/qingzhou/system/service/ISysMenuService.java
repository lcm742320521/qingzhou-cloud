package com.qingzhou.system.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.common.core.web.domain.entity.SysUser;
import com.qingzhou.system.domain.SysMenu;
import com.qingzhou.system.domain.vo.RouterVo;
import com.qingzhou.system.domain.vo.TreeSelect;

import java.util.List;
import java.util.Set;

/**
 * 系统菜单 服务层
 * @author xm
 */
public interface ISysMenuService extends IService<SysMenu> {

    /**
     * 根据用户查询系统菜单列表
     * @param menu 菜单信息
     * @param userId 用户ID
     * @return
     */
    List<SysMenu> selectMenuList(SysMenu menu, Long userId);

    /**
     * 构建前端所需要下拉树结构
     * @param menus 菜单列表
     * @return
     */
    List<TreeSelect> buildMenuTreeSelect(List<SysMenu> menus);

    /**
     * 根据用户查询系统菜单列表
     * @param userId 用户ID
     * @return
     */
    List<SysMenu> selectMenuList(Long userId);

    /**
     * 根据用户 ID查询菜单树信息
     * @param userId
     * @return
     */
    List<SysMenu> selectMenuTreeByUserId(Long userId);

    /**
     * 根据角色ID查询菜单树信息
     * @param roleId 角色ID
     * @return
     */
    List<Long> selectMenuListByRoleId(Long roleId);

    /**
     * 构建前端路由所需要的菜单
     * @param menus
     * @return
     */
    List<RouterVo> buildMenus(List<SysMenu> menus);

    /**
     * 获取菜单数据权限
     * @param sysUser
     * @return
     */
    Set<String> getMenuPermission(SysUser sysUser);

    /**
     * 新增系统菜单
     * @param sysMenu
     * @return
     */
    int add(SysMenu sysMenu);

    /**
     * 修改系统菜单
     * @param sysMenu
     * @return
     */
    int edit(SysMenu sysMenu);

    /**
     * 删除系统菜单
     * @param menuId
     */
    int delete(Long menuId);

}
