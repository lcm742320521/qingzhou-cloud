package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.domain.SysRoleMenu;

/**
 * 角色与菜单关联 数据层
 * @author xm
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

    /**
     * 查询菜单使用数量
     * @param menuId 菜单ID
     * @return
     */
    long checkMenuExistRole(Long menuId);

    /**
     * 通过角色ID删除角色和菜单关联
     * @param roleId 角色ID
     * @return
     */
    int deleteRoleMenuByRoleId(Long roleId);

    /**
     * 批量删除角色菜单关联信息
     * @param ids 需要删除的数据ID
     * @return
     */
    int deleteRoleMenu(Long[] ids);

}
