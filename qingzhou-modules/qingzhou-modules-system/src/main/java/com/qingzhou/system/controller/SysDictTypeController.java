package com.qingzhou.system.controller;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.core.utils.ExcelUtil;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.core.web.domain.PageData;
import com.qingzhou.system.domain.SysDictType;
import com.qingzhou.system.service.ISysDictTypeService;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.data.annotation.Tran;
import org.noear.solon.validation.annotation.Validated;

import java.util.Arrays;
import java.util.List;

/**
 * 数据字典类型 操作处理
 * @author xm
 */
@Controller
@Mapping("dict/type")
public class SysDictTypeController extends BaseController {

    @Inject
    ISysDictTypeService sysDictTypeService;

    /**
     * 查询数据字典类型列表
     */
    @AuthPermissions("system:dict:list")
    @Get
    @Mapping("list")
    public PageData<SysDictType> list(Page<SysDictType> page, SysDictType sysDictType) {
        QueryWrapper qw = getQW(sysDictType);
        Page<SysDictType> result = sysDictTypeService.page(page, qw);
        return getPageData(result);
    }

    /**
     * 导出数据字典类型列表
     */
    @QzLog(title = "数据字典类型", businessType = BusinessType.EXPORT)
    @AuthPermissions("system:dict:export")
    @Post
    @Mapping("export")
    public void export(SysDictType sysDictType) {
        QueryWrapper qw = getQW(sysDictType);
        List<SysDictType> list = sysDictTypeService.list(qw);
        ExcelUtil.export(list);
    }

    /**
     * 查询数据字典类型详情
     */
    @AuthPermissions("system:dict:query")
    @Get
    @Mapping("{id}")
    public SysDictType info(@Path Long id) {
        return sysDictTypeService.getById(id);
    }

    /**
     * 新增数据字典类型
     */
    @QzLog(title = "数据字典类型", businessType = BusinessType.INSERT)
    @AuthPermissions("system:dict:add")
    @Post
    @Mapping
    public Result<Void> add(@Body @Validated SysDictType sysDictType) {
        return toResult(sysDictTypeService.add(sysDictType));
    }

    /**
     * 修改数据字典类型
     */
    @QzLog(title = "数据字典类型", businessType = BusinessType.UPDATE)
    @AuthPermissions("system:dict:edit")
    @Tran
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated SysDictType sysDictType) {
        return toResult(sysDictTypeService.edit(sysDictType));
    }

    /**
     * 删除数据字典类型
     */
    @QzLog(title = "数据字典类型", businessType = BusinessType.DELETE)
    @AuthPermissions("system:dict:remove")
    @Delete
    @Mapping("{dictIds}")
    public Result<Void> delete(@Path Long[] dictIds) {
        sysDictTypeService.delete(Arrays.asList(dictIds));
        return Result.succeed();
    }

    /**
     * 获取字典选择框列表
     */
    @Get
    @Mapping("optionselect")
    public List<SysDictType> optionselect() {
        return sysDictTypeService.list();
    }

    private QueryWrapper getQW(SysDictType sysDictType) {
        QueryWrapper qw = QueryWrapper.create();
        qw.and(SysDictType::getDictName).like(sysDictType.getDictName());
        qw.and(SysDictType::getDictType).like(sysDictType.getDictType());
        qw.and(SysDictType::getStatus).eq(sysDictType.getStatus());
        return qw;
    }

}
