package com.qingzhou.system.controller;

import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.security.utils.SecurityUtil;
import com.qingzhou.system.domain.SysMenu;
import com.qingzhou.system.domain.vo.RouterVo;
import com.qingzhou.system.domain.vo.TreeSelect;
import com.qingzhou.system.service.ISysMenuService;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.Validated;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统菜单 操作处理
 * @author xm
 */
@Controller
@Mapping("menu")
public class SysMenuController extends BaseController {

    @Inject
    ISysMenuService sysMenuService;

    /**
     * 查询系统菜单列表
     */
    @AuthPermissions("system:menu:list")
    @Get
    @Mapping("list")
    public List<SysMenu> list(SysMenu menu) {
        Long userId = SecurityUtil.getUserId();
        return sysMenuService.selectMenuList(menu, userId);
    }

    /**
     * 查询系统菜单详情
     */
    @AuthPermissions("system:menu:query")
    @Get
    @Mapping("{id}")
    public SysMenu info(@Path Long id) {
        return sysMenuService.getById(id);
    }

    /**
     * 获取菜单下拉树列表
     * @param menu
     * @return
     */
    @Get
    @Mapping("treeselect")
    public List<TreeSelect> treeselect(SysMenu menu) {
        Long userId = SecurityUtil.getUserId();
        List<SysMenu> menus = sysMenuService.selectMenuList(menu, userId);
        return sysMenuService.buildMenuTreeSelect(menus);
    }

    /**
     * 加载对应角色菜单列表树
     */
    @Get
    @Mapping("roleMenuTreeselect/{roleId}")
    public Map<String, Object> roleMenuTreeselect(@Path("roleId") Long roleId) {
        Long userId = SecurityUtil.getUserId();
        List<SysMenu> menus = sysMenuService.selectMenuList(userId);
        Map<String, Object> map = new HashMap<>();
        map.put("checkedKeys", sysMenuService.selectMenuListByRoleId(roleId));
        map.put("menus", sysMenuService.buildMenuTreeSelect(menus));
        return map;
    }

    /**
     * 新增系统菜单
     */
    @QzLog(title = "系统菜单", businessType = BusinessType.INSERT)
    @AuthPermissions("system:menu:add")
    @Post
    @Mapping
    public Result<Void> add(@Body @Validated SysMenu sysMenu) {
        return toResult(sysMenuService.add(sysMenu));
    }

    /**
     * 修改系统菜单
     */
    @QzLog(title = "系统菜单", businessType = BusinessType.UPDATE)
    @AuthPermissions("system:menu:edit")
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated SysMenu sysMenu) {
        return toResult(sysMenuService.edit(sysMenu));
    }

    /**
     * 删除系统菜单
     */
    @QzLog(title = "系统菜单", businessType = BusinessType.DELETE)
    @AuthPermissions("system:menu:remove")
    @Delete
    @Mapping("{menuId}")
    public Result<Void> delete(@Path Long menuId) {
        return toResult(sysMenuService.delete(menuId));
    }

    /**
     * 获取路由信息
     * @return
     */
    @Get
    @Mapping("getRouters")
    public List<RouterVo> getRouters() {
        Long userId = SecurityUtil.getUserId();
        List<SysMenu> menus = sysMenuService.selectMenuTreeByUserId(userId);
        return sysMenuService.buildMenus(menus);
    }

}
