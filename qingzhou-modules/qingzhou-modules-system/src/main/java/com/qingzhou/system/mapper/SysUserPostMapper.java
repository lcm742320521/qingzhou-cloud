package com.qingzhou.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.qingzhou.system.domain.SysUserPost;

/**
 * 用户和岗位关联 数据层
 * @author xm
 */
public interface SysUserPostMapper extends BaseMapper<SysUserPost> {

    /**
     * 通过岗位ID查询岗位使用数量
     * @param postId 岗位ID
     * @return
     */
    long countUserPostByPostId(Long postId);

    /**
     * 通过用户ID删除用户和岗位关联
     * @param userId 用户ID
     * @return
     */
    int deleteUserPostByUserId(Long userId);

    /**
     * 批量删除用户和岗位关联
     * @param userIds 需要删除的用户ID
     * @return
     */
    int deleteUserPost(Long[] userIds);

}
