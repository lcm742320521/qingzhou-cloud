package com.qingzhou.system.domain;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import com.qingzhou.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统菜单 对象
 * @author xm
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(value = "sys_menu")
public class SysMenu extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 菜单ID */
    @Id
    private Long menuId;

    /** 菜单名称 */
    @NotBlank(message = "菜单名称不能为空")
    @Length(max = 50, message = "菜单名称长度不能超过50个字符")
    private String menuName;

    /** 父菜单ID */
    private Long parentId;

    /** 父菜单名称 */
    @Column(ignore = true)
    private String parentName;

    /** 显示顺序 */
    @NotNull(message = "显示顺序不能为空")
    private Integer orderNum;

    /** 路由地址 */
    @Length(max = 200, message = "路由地址不能超过200个字符")
    private String path;

    /** 组件路径 */
    @Length(max = 200, message = "组件路径不能超过200个字符")
    private String component;

    /** 路由参数 */
    private String query;

    /** 是否为外链（Y是 N否） */
    private String isFrame;

    /** 是否缓存（Y缓存 N不缓存） */
    private String isCache;

    /** 类型（M目录 C菜单 F按钮） */
    @NotBlank(message = "菜单名称不能为空")
    private String menuType;

    /** 显示状态（0显示 1隐藏） */
    private String visible;
    
    /** 菜单状态（0正常 1停用） */
    private String status;

    /** 权限字符串 */
    @Length(max = 100, message = "权限标识长度不能超过100个字符")
    private String perms;

    /** 菜单图标 */
    private String icon;

    /** 子菜单 */
    @Column(ignore = true)
    private List<SysMenu> children = new ArrayList<>();

}
