package com.qingzhou.system.controller;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.core.utils.ExcelUtil;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.core.web.domain.PageData;
import com.qingzhou.system.domain.SysDictData;
import com.qingzhou.system.service.ISysDictDataService;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.Validated;

import java.util.Arrays;
import java.util.List;

/**
 * 数据字典 操作处理
 * @author xm
 */
@Controller
@Mapping("dict/data")
public class SysDictDataController extends BaseController {

    @Inject
    ISysDictDataService sysDictDataService;

    /**
     * 查询数据字典列表
     */
    @AuthPermissions("system:dict:list")
    @Get
    @Mapping("list")
    public PageData<SysDictData> list(Page<SysDictData> page, SysDictData sysDictData) {
        QueryWrapper qw = getQW(sysDictData);
        Page<SysDictData> result = sysDictDataService.page(page, qw);
        return getPageData(result);
    }

    /**
     * 导出数据字典列表
     */
    @QzLog(title = "数据字典", businessType = BusinessType.EXPORT)
    @AuthPermissions("system:dict:export")
    @Post
    @Mapping("export")
    public void export(SysDictData sysDictData) {
        QueryWrapper qw = getQW(sysDictData);
        List<SysDictData> list = sysDictDataService.list(qw);
        ExcelUtil.export(list);
    }

    /**
     * 查询数据字典详情
     */
    @AuthPermissions("system:dict:query")
    @Get
    @Mapping("{id}")
    public SysDictData info(@Path Long id) {
        return sysDictDataService.getById(id);
    }

    /**
     * 根据字典类型查询数据字典
     */
    @Get
    @Mapping("type/{dictType}")
    public List<SysDictData> getDataByType(@Path String dictType) {
        return sysDictDataService.getDataByType(dictType);
    }

    /**
     * 新增数据字典
     */
    @QzLog(title = "数据字典", businessType = BusinessType.INSERT)
    @AuthPermissions("system:dict:add")
    @Post
    @Mapping
    public Result<Void> add(@Body @Validated SysDictData sysDictData) {
        return toResult(sysDictDataService.add(sysDictData));
    }

    /**
     * 修改数据字典
     */
    @QzLog(title = "数据字典", businessType = BusinessType.UPDATE)
    @AuthPermissions("system:dict:edit")
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated SysDictData sysDictData) {
        return toResult(sysDictDataService.edit(sysDictData));
    }

    /**
     * 删除数据字典
     */
    @QzLog(title = "数据字典", businessType = BusinessType.DELETE)
    @AuthPermissions("system:dict:remove")
    @Delete
    @Mapping("{dictCodes}")
    public Result<Void> delete(@Path Long[] dictCodes) {
        sysDictDataService.delete(Arrays.asList(dictCodes));
        return Result.succeed();
    }

    /**
     * 刷新字典缓存
     */
    @QzLog(title = "数据字典", businessType = BusinessType.CLEAN)
    @AuthPermissions("system:dict:remove")
    @Get
    @Mapping("refresh/cache")
    public Result<Void> refreshCache() {
        sysDictDataService.loadingDictCache();
        return Result.succeed();
    }

    private QueryWrapper getQW(SysDictData sysDictData) {
        QueryWrapper qw = QueryWrapper.create();
        qw.and(SysDictData::getDictType).eq(sysDictData.getDictType());
        qw.and(SysDictData::getDictLabel).like(sysDictData.getDictLabel());
        qw.and(SysDictData::getStatus).eq(sysDictData.getStatus());
        return qw;
    }

}
