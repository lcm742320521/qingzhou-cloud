package com.qingzhou.system.controller;

import com.mybatisflex.core.paginate.Page;
import com.qingzhou.common.core.constants.UserConstant;
import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.core.utils.ExcelUtil;
import com.qingzhou.common.datasource.annotation.QzDataScope;
import com.qingzhou.common.datasource.annotation.QzLog;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.common.core.web.domain.PageData;
import com.qingzhou.common.core.web.domain.entity.SysRole;
import com.qingzhou.system.domain.SysUserRole;
import com.qingzhou.system.service.ISysRoleService;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.data.annotation.Tran;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
 * 角色 操作处理
 * @author xm
 */
@Controller
@Mapping("role")
public class SysRoleController extends BaseController {

    @Inject
    ISysRoleService sysRoleService;

    /**
     * 查询角色列表
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:role:list")
    @Get
    @Mapping("list")
    public PageData<SysRole> list(Page<SysRole> page, SysRole sysRole) {
        startPage(page);
        List<SysRole> list = sysRoleService.selectRoleList(sysRole);
        return getPageData(list);
    }

    /**
     * 导出角色列表
     */
    @QzLog(title = "角色", businessType = BusinessType.EXPORT)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:role:export")
    @Post
    @Mapping("export")
    public void export(SysRole sysRole) {
        List<SysRole> list = sysRoleService.selectRoleList(sysRole);
        ExcelUtil.export(list);
    }

    /**
     * 查询角色详情
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:role:query")
    @Get
    @Mapping("{id}")
    public SysRole info(@Path Long id, SysRole sysRole) {
        return sysRoleService.info(id, sysRole);
    }

    /**
     * 新增角色
     */
    @QzLog(title = "角色", businessType = BusinessType.INSERT)
    @AuthPermissions("system:role:add")
    @Tran
    @Post
    @Mapping
    public Result<Void> add(@Body @Validated SysRole sysRole) {
        return toResult(sysRoleService.add(sysRole));
    }

    /**
     * 修改角色
     */
    @QzLog(title = "角色", businessType = BusinessType.UPDATE)
    @AuthPermissions("system:role:edit")
    @Tran
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated SysRole sysRole) {
        return toResult(sysRoleService.edit(sysRole));
    }

    /**
     * 修改保存数据权限
     */
    @QzLog(title = "角色", businessType = BusinessType.UPDATE)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:role:edit")
    @Tran
    @Put
    @Mapping("dataScope")
    public Result<Void> dataScope(@Body SysRole sysRole) {
        return toResult(sysRoleService.dataScope(sysRole));
    }

    /**
     * 状态修改
     */
    @QzLog(title = "角色", businessType = BusinessType.UPDATE)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:role:edit")
    @Put
    @Mapping("changeStatus")
    public Result<Void> changeStatus(@Body SysRole sysRole) {
        return toResult(sysRoleService.changeStatus(sysRole));
    }

    /**
     * 删除角色
     */
    @QzLog(title = "角色", businessType = BusinessType.DELETE)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:role:remove")
    @Tran
    @Delete
    @Mapping("{roleIds}")
    public Result<Void> delete(@Path Long[] roleIds, SysRole sysRole) {
        return toResult(sysRoleService.delete(roleIds, sysRole));
    }

    /**
     * 获取角色选择框列表
     */
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:role:query")
    @Get
    @Mapping("optionselect")
    public List<SysRole> optionselect(SysRole sysRole) {
        return sysRoleService.selectRoleList(sysRole);
    }

    /**
     * 取消授权用户
     */
    @QzLog(title = "角色", businessType = BusinessType.GRANT)
    @AuthPermissions("system:role:edit")
    @Put
    @Mapping("authUser/cancel")
    public Result<Void> cancelAuthUser(@Body SysUserRole sysUserRole) {
        return toResult(sysRoleService.cancelAuthUser(sysUserRole));
    }

    /**
     * 批量取消授权用户
     */
    @QzLog(title = "角色", businessType = BusinessType.GRANT)
    @AuthPermissions("system:role:edit")
    @Put
    @Mapping("authUser/cancelAll")
    public Result<Void> cancelAuthUserAll(Long roleId, Long[] userIds) {
        return toResult(sysRoleService.cancelAuthUsers(roleId, userIds));
    }

    /**
     * 批量选择用户授权
     */
    @QzLog(title = "角色", businessType = BusinessType.GRANT)
    @QzDataScope(deptAlias = UserConstant.DATA_SCOPE_ALIAS_DEPT)
    @AuthPermissions("system:role:edit")
    @Put
    @Mapping("authUser/selectAll")
    public Result<Void> selectAuthUserAll(Long roleId, Long[] userIds, SysRole sysRole) {
        return toResult(sysRoleService.insertAuthUsers(roleId, userIds, sysRole));
    }

}
