package com.qingzhou.demo.controller;

import com.qingzhou.common.core.enums.BusinessType;
import com.qingzhou.common.datasource.annotation.QzLog;
import org.noear.solon.annotation.*;
import org.noear.solon.auth.annotation.AuthPermissions;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.Validated;
import com.mybatisflex.core.query.QueryWrapper;
import com.qingzhou.common.core.utils.ExcelUtil;
import com.qingzhou.common.core.web.controller.BaseController;
import com.qingzhou.demo.domain.DemoTest;
import com.qingzhou.demo.service.IDemoTestService;
import com.mybatisflex.core.paginate.Page;
import com.qingzhou.common.core.web.domain.PageData;

import java.util.Arrays;
import java.util.List;

/**
 * 演示 操作处理
 * @author xm
 */
@Controller
@Mapping("test")
public class DemoTestController extends BaseController {

    @Inject
    IDemoTestService demoTestService;

    /**
     * 查询演示列表
     */
    @AuthPermissions("demo:test:list")
    @Get
    @Mapping("list")
    public PageData<DemoTest> list(Page<DemoTest> page, DemoTest demoTest) {
        QueryWrapper qw = getQW(demoTest);
        Page<DemoTest> result = demoTestService.page(page, qw);
        return getPageData(result);
    }

    /**
     * 导出演示列表
     */
    @QzLog(title = "演示", businessType = BusinessType.EXPORT)
    @AuthPermissions("demo:test:export")
    @Post
    @Mapping("export")
    public void export(DemoTest demoTest) {
        QueryWrapper qw = getQW(demoTest);
        List<DemoTest> list = demoTestService.list(qw);
        ExcelUtil.export(list);
    }

    /**
     * 查询演示详情
     */
    @AuthPermissions("demo:test:query")
    @Get
    @Mapping("{id}")
    public DemoTest info(@Path Long id) {
        return demoTestService.getById(id);
    }

    /**
     * 新增演示
     */
    @QzLog(title = "演示", businessType = BusinessType.INSERT)
    @AuthPermissions("demo:test:add")
    @Post
    @Mapping
    public Result<Void> add(@Body @Validated DemoTest demoTest) {
        return toResult(demoTestService.save(demoTest));
    }

    /**
     * 修改演示
     */
    @QzLog(title = "演示", businessType = BusinessType.UPDATE)
    @AuthPermissions("demo:test:edit")
    @Put
    @Mapping
    public Result<Void> edit(@Body @Validated DemoTest demoTest) {
        return toResult(demoTestService.updateById(demoTest));
    }

    /**
     * 删除演示
     */
    @QzLog(title = "演示", businessType = BusinessType.DELETE)
    @AuthPermissions("demo:test:remove")
    @Delete
    @Mapping("{ids}")
    public Result<Void> delete(@Path Long[] ids) {
        return toResult(demoTestService.removeByIds(Arrays.asList(ids)));
    }

    private QueryWrapper getQW(DemoTest demoTest) {
        QueryWrapper qw = QueryWrapper.create();
        qw.and(DemoTest::getName).like(demoTest.getName());
        qw.and(DemoTest::getAge).eq(demoTest.getAge());
        qw.and(DemoTest::getBirthday).eq(demoTest.getBirthday());
        qw.and(DemoTest::getLiveAddress).eq(demoTest.getLiveAddress());
        return qw;
    }

}
