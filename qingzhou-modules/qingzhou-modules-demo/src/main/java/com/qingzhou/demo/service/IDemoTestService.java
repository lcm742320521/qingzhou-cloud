package com.qingzhou.demo.service;

import com.mybatisflex.core.service.IService;
import com.qingzhou.demo.domain.DemoTest;

/**
 * 演示 服务层
 * @author xm
 */
public interface IDemoTestService extends IService<DemoTest> {

}
