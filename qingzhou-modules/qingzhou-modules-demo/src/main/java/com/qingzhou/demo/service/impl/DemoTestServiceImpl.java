package com.qingzhou.demo.service.impl;

import com.mybatisflex.solon.service.impl.ServiceImpl;
import com.qingzhou.demo.domain.DemoTest;
import com.qingzhou.demo.mapper.DemoTestMapper;
import com.qingzhou.demo.service.IDemoTestService;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.annotation.Component;

/**
 * 演示 服务层实现
 * @author xm
 */
@Component
public class DemoTestServiceImpl extends ServiceImpl<DemoTestMapper, DemoTest> implements IDemoTestService {

    @Db
    DemoTestMapper demoTestMapper;

}
