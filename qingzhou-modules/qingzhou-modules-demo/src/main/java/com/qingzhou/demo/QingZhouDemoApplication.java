package com.qingzhou.demo;

import org.noear.solon.Solon;
import org.noear.solon.admin.client.annotation.EnableAdminClient;
import org.noear.solon.annotation.SolonMain;

/**
 * 演示模块
 * @author xm
 */
//@EnableAdminClient
@SolonMain
public class QingZhouDemoApplication {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Solon.start(QingZhouDemoApplication.class, args);
        long times = System.currentTimeMillis() - start;
        System.out.println("(♥◠‿◠)ﾉﾞ  演示模块启动成功，耗时:【" + times + "ms】  ლ(´ڡ`ლ)ﾞ");
    }

}
