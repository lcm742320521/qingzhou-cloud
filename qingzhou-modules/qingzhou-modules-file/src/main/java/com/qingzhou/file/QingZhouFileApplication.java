package com.qingzhou.file;

import org.noear.solon.Solon;
import org.noear.solon.admin.client.annotation.EnableAdminClient;
import org.noear.solon.annotation.SolonMain;

/**
 * 文件模块
 * @author xm
 */
//@EnableAdminClient
@SolonMain
public class QingZhouFileApplication {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Solon.start(QingZhouFileApplication.class, args);
        long times = System.currentTimeMillis() - start;
        System.out.println("(♥◠‿◠)ﾉﾞ  文件模块启动成功，耗时:【" + times + "ms】  ლ(´ڡ`ლ)ﾞ");
    }

}
