package com.qingzhou.system.api;

import com.qingzhou.common.core.constants.HttpConstant;
import com.qingzhou.common.core.constants.ServiceConstant;
import com.qingzhou.common.core.web.nami.INamiClientFilter;
import com.qingzhou.common.core.web.domain.model.LoginUser;
import com.qingzhou.common.core.web.domain.entity.SysUser;
import org.noear.nami.annotation.NamiClient;
import org.noear.nami.annotation.NamiMapping;
import org.noear.solon.core.handle.Result;

/**
 * 远程调用系统服务 - 用户
 * @author xm
 */
@NamiClient(name = ServiceConstant.SERVICE_SYSTEM, path = ServiceConstant.PATH_TO_SYSTEM_USER)
public interface RemoteSysUserService extends INamiClientFilter {

    /**
     * 根据账号查询用户
     * @param userName
     */
    @NamiMapping(HttpConstant.HTTP_GET)
    Result<LoginUser> getSysUserByUserName(String userName);

    /**
     * 校验账号是否唯一
     * @param sysUser
     * @return
     */
    Result<Boolean> checkUserNameUnique(SysUser sysUser);

    /**
     * 注册用户
     * @param sysUser
     * @return
     */
    Result<Integer> registerUser(SysUser sysUser);

}
