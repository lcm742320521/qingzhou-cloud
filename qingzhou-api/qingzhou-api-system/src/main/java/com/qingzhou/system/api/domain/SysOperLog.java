package com.qingzhou.system.api.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import com.qingzhou.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 操作日志 对象
 * @author xm
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(value = "sys_oper_log")
public class SysOperLog extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 操作序号 */
    @Excel(name = "操作序号")
    @Id
    private Long operId;

    /** 操作模块 */
    @Excel(name = "操作模块")
    private String title;

    /** 业务类型（0其它 1新增 2修改 3删除） */
    @Excel(name = "业务类型", replace = { "其它_0", "新增_1", "修改_2", "删除_3" })
    private Integer businessType;

    /** 请求方法 */
    @Excel(name = "请求方法")
    private String method;

    /** 请求方式 */
    @Excel(name = "请求方式")
    private String requestMethod;

    /** 操作类别（00 PC端用户 01 APP端用户） */
    @Excel(name = "操作类别", replace = { "PC端用户_00", "APP端用户_01" })
    private String operatorType;

    /** 操作人员 */
    @Excel(name = "操作人员")
    private String operName;

    /** 请求url */
    @Excel(name = "请求地址")
    private String operUrl;

    /** 操作地址 */
    @Excel(name = "操作地址")
    private String operIp;

    /** 请求参数 */
    @Excel(name = "请求参数")
    private String operParam;

    /** 返回参数 */
    @Excel(name = "返回参数")
    private String jsonResult;

    /** 操作状态（0成功 1失败） */
    @Excel(name = "操作状态", replace = { "成功_0", "失败_1" })
    private String status;

    /** 错误消息 */
    @Excel(name = "错误消息")
    private String errorMsg;

    /** 消耗时间 */
    @Excel(name = "消耗时间")
    private Long costTime;

    @Column(ignore = true)
    private Date startTime;

    @Column(ignore = true)
    private Date endTime;

}
