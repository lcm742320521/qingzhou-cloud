import Element from 'element-ui'
import Cookies from 'js-cookie'
import VueMeta from 'vue-meta'

import '@/assets/styles/element-variables.scss'
import '@/assets/styles/index.scss'
import '@/assets/styles/qingzhou.scss'
import '@/assets/icons'

import DictData from '@/components/DictData'
import DictTag from '@/components/DictTag'
import Editor from '@/components/Editor'
import FileUpload from '@/components/FileUpload'
import ImagePreview from '@/components/ImagePreview'
import ImageUpload from '@/components/ImageUpload'
import Pagination from '@/components/Pagination'
import RightToolbar from '@/components/RightToolbar'

import directives from '@/directives'
import cache from '@/utils/cache'
import { parseTime } from '@/utils/date'
import { download } from '@/utils/download'
import modal from '@/utils/modal'
import tab from '@/utils/tab'
import { resetForm, addDateRange, selectDictLabel, handleTree } from '@/utils'

export default {
  install(Vue) {
    // 全局方法挂载
    Vue.prototype.$cache = cache // 缓存对象
    Vue.prototype.$parseTime = parseTime // 格式化时间
    Vue.prototype.$download = download // 下载文件
    Vue.prototype.$modal = modal // 模态框对象
    Vue.prototype.$tab = tab // 页签操作
    Vue.prototype.$resetForm = resetForm // 表单重置
    Vue.prototype.$addDateRange = addDateRange // 添加日期范围
    Vue.prototype.$selectDictLabel = selectDictLabel // 回显数据字典
    Vue.prototype.$handleTree = handleTree // 构造树型结构数据

    // 全局组件挂载
    Vue.component('DictTag', DictTag) // 字典标签组件
    Vue.component('Editor', Editor) // 富文本组件
    Vue.component('FileUpload', FileUpload) // 文件上传组件
    Vue.component('ImagePreview', ImagePreview) // 图片预览组件
    Vue.component('ImageUpload', ImageUpload) // 图片上传组件
    Vue.component('Pagination', Pagination) // 分页组件
    Vue.component('RightToolbar', RightToolbar) // 自定义表格工具组件

    // 应用插件
    Vue.use(VueMeta) // 头部标签组件
    Vue.use(directives) // 自定义指令
    Vue.use(Element, {
      size: Cookies.get('size') || 'medium' // set element-ui default size
    })

    // 关闭生产提示
    Vue.config.productionTip = false

    // 字典数据组件
    DictData.install()
  }
}
