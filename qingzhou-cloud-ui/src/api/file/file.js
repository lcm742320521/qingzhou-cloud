import request from '@/utils/request'

// 单文件上传
export function uploadFile(file) {
  return request({
    url: '/file/upload/file',
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    method: 'post',
    data: file
  })
}
