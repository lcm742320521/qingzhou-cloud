import request from '@/utils/request'

// 查询登录日志列表
export function listLoginLog(query) {
  return request({
    url: '/system/loginlog/list',
    method: 'get',
    params: query
  })
}

// 解锁用户登录状态
export function unlockLoginLog(userName) {
  return request({
    url: '/system/loginlog/unlock/' + userName,
    method: 'get'
  })
}

// 删除登录日志
export function delLoginLog(infoId) {
  return request({
    url: '/system/loginlog/' + infoId,
    method: 'delete'
  })
}

// 清空登录日志
export function cleanLoginLog() {
  return request({
    url: '/system/loginlog/clean',
    method: 'delete'
  })
}
