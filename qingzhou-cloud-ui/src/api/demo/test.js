import request from '@/utils/request'

// 查询演示列表
export function listTest(query) {
  return request({
    url: '/demo/test/list',
    method: 'get',
    params: query
  })
}

// 查询演示详细
export function getTest(id) {
  return request({
    url: '/demo/test/' + id,
    method: 'get'
  })
}

// 新增演示
export function addTest(data) {
  return request({
    url: '/demo/test',
    method: 'post',
    data: data
  })
}

// 修改演示
export function updateTest(data) {
  return request({
    url: '/demo/test',
    method: 'put',
    data: data
  })
}

// 删除演示
export function delTest(id) {
  return request({
    url: '/demo/test/' + id,
    method: 'delete'
  })
}
