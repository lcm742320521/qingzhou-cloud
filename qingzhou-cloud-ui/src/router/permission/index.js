import { Message } from 'element-ui'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import store from '@/store'
import { getToken } from '@/utils/auth'
import { isRelogin } from '@/utils/request'
import { isHttp } from '@/utils/validate'

// 进度条
NProgress.configure({ showSpinner: false })

const whiteList = ['/login', '/register']

/**
 * 基于权限生成动态路由
 * @param {*} router
 */
export function permission(router) {
  // 前置路由守卫
  router.beforeEach((to, from, next) => {
    NProgress.start()
    if (getToken()) {
      to.meta.title && store.dispatch('settings/setTitle', to.meta.title)
      /* 有token */
      if (to.path === '/login') {
        next({ path: '/' })
        NProgress.done()
      } else {
        if (store.getters.roles.length === 0) {
          isRelogin.show = true
          // 判断当前用户是否已拉取完user_info信息
          store.dispatch('GetUserInfo').then(() => {
            isRelogin.show = false
            store.dispatch('GenerateRoutes').then(accessRoutes => {
              // 根据roles权限生成可访问的路由表
              accessRoutes.forEach(route => {
                if (!isHttp(route.path)) {
                  // 动态添加可访问路由表
                  router.addRoute(route)
                }
              })
              // hack方法 确保addRoute已完成
              next({ ...to, replace: true })
            })
          }).catch(err => {
            store.dispatch('LogOut').then(() => {
              Message.error(err)
              next({ path: '/' })
            })
          })
        } else {
          next()
        }
      }
    } else {
      // 没有token
      if (whiteList.indexOf(to.path) !== -1) {
        // 在免登录白名单，直接进入
        next()
      } else {
        // 否则全部重定向到登录页
        next(`/login?redirect=${encodeURIComponent(to.fullPath)}`)
        NProgress.done()
      }
    }
  })

  // 后置路由守卫
  router.afterEach(() => {
    NProgress.done()
  })
}
