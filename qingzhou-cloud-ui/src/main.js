import Vue from 'vue'
import plugins from './plugins'
import router from './router'
import store from './store'
import App from './App'

Vue.use(plugins)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
