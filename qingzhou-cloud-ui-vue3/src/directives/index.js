import clipboard from './module/clipboard'
import hasPermi from './permission/hasPermi'
import hasRole from './permission/hasRole'

export default function directives(app) {
  app.directive('clipboard', clipboard)
  app.directive('has-permi', hasPermi)
  app.directive('has-role', hasRole)
}
