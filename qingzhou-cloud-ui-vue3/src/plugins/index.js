import ElementPlus from 'element-plus'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import Cookies from 'js-cookie'

import '@/assets/styles/index.scss'
import 'virtual:svg-icons-register'
import SvgIcon from '@/components/SvgIcon'
import elementIcons from '@/components/SvgIcon/svgicon'

import DictTag from '@/components/DictTag'
import Editor from '@/components/Editor'
import FileUpload from '@/components/FileUpload'
import ImagePreview from '@/components/ImagePreview'
import ImageUpload from '@/components/ImageUpload'
import Pagination from '@/components/Pagination'
import RightToolbar from '@/components/RightToolbar'
import TreeSelect from '@/components/TreeSelect'

import directives from '@/directives'
import router from '@/router'
import store from '@/store'
import { getConfigKey } from '@/api/system/config'
import cache from '@/utils/cache'
import { parseTime } from '@/utils/date'
import { useDict } from '@/utils/dict'
import { download } from '@/utils/download'
import modal from '@/utils/modal'
import tab from '@/utils/tab'
import { resetForm, addDateRange, selectDictLabel, handleTree } from '@/utils'

export default {
  install: (app) => {
    // 全局方法挂载
    app.config.globalProperties.$getConfigKey = getConfigKey
    app.config.globalProperties.$cache = cache // 缓存对象
    app.config.globalProperties.$parseTime = parseTime // 格式化时间
    app.config.globalProperties.$useDict = useDict // 获取字典数据
    app.config.globalProperties.$download = download // 下载文件
    app.config.globalProperties.$modal = modal // 模态框对象
    app.config.globalProperties.$tab = tab // 页签操作
    app.config.globalProperties.$resetForm = resetForm // 表单重置
    app.config.globalProperties.$addDateRange = addDateRange // 添加日期范围
    app.config.globalProperties.$selectDictLabel = selectDictLabel // 回显数据字典
    app.config.globalProperties.$handleTree = handleTree // 构造树型结构数据

    // 全局组件挂载
    app.component('SvgIcon', SvgIcon)
    app.component('DictTag', DictTag) // 字典标签组件
    app.component('Editor', Editor) // 富文本组件
    app.component('FileUpload', FileUpload) // 文件上传组件
    app.component('ImagePreview', ImagePreview) // 图片预览组件
    app.component('ImageUpload', ImageUpload) // 图片上传组件
    app.component('Pagination', Pagination) // 分页组件
    app.component('RightToolbar', RightToolbar) // 自定义表格工具组件
    app.component('TreeSelect', TreeSelect) // 自定义树选择组件

    // 自定义指令
    directives(app)

    // 应用插件
    app.use(router)
    app.use(store)
    app.use(elementIcons)
    app.use(ElementPlus, {
      locale: zhCn, // 中文语言
      size: Cookies.get('size') || 'default' // 支持 large、default、small
    })

    // 修改 el-dialog 默认点击遮罩为不关闭
    app._context.components.ElDialog.props.closeOnClickModal.default = false
  }
}
