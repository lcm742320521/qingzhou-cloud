import { ElMessage } from 'element-plus'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import useUserStore from '@/store/modules/user'
import useSettingsStore from '@/store/modules/settings'
import usePermissionStore from '@/store/modules/permission'
import { getToken } from '@/utils/auth'
import { isHttp } from '@/utils/validate'
import { isRelogin } from '@/utils/request'

// 进度条
NProgress.configure({ showSpinner: false })

const whiteList = ['/login', '/register']

/**
 * 基于权限生成动态路由
 * @param {*} router
 */
export function permission(router) {
  // 前置路由守卫
  router.beforeEach((to, from, next) => {
    NProgress.start()
    if (getToken()) {
      to.meta.title && useSettingsStore().setTitle(to.meta.title)
      /* 有token */
      if (to.path === '/login') {
        next({ path: '/' })
        NProgress.done()
      } else {
        if (useUserStore().roles.length === 0) {
          isRelogin.show = true
          // 判断当前用户是否已拉取完user_info信息
          useUserStore().getUserInfo().then(() => {
            isRelogin.show = false
            usePermissionStore().generateRoutes().then(accessRoutes => {
              // 根据roles权限生成可访问的路由表
              accessRoutes.forEach(route => {
                if (!isHttp(route.path)) {
                  // 动态添加可访问路由表
                  router.addRoute(route)
                }
              })
              // hack方法 确保addRoutes已完成
              next({ ...to, replace: true })
            })
          }).catch(err => {
            useUserStore().logOut().then(() => {
              ElMessage.error(err)
              next({ path: '/' })
            })
          })
        } else {
          next()
        }
      }
    } else {
      // 没有token
      if (whiteList.indexOf(to.path) !== -1) {
        // 在免登录白名单，直接进入
        next()
      } else {
        // 否则全部重定向到登录页
        next(`/login?redirect=${to.fullPath}`)
        NProgress.done()
      }
    }
  })

  // 后置路由守卫
  router.afterEach(() => {
    NProgress.done()
  })
}
