<p align="center">
    <img src="https://gitee.com/opensolon/qingzhou-cloud/raw/master/qingzhou-cloud-ui/src/assets/images/logo/logo.png" alt="" />
</p>

<h1 align="center">QingZhou Cloud</h1>
<h3 align="center">v3.0.1</h3>

<h4 align="center">基于 Solon 和 Vue 前后端分离的分布式微服务架构</h4>

<p align="center">
	<a href="https://gitee.com/opensolon/qingzhou-cloud/stargazers">
        <img src="https://gitee.com/opensolon/qingzhou-cloud/badge/star.svg?theme=dark" />
    </a>
    <a href="https://gitee.com/opensolon/qingzhou-cloud/blob/master/LICENSE">
        <img src="https://img.shields.io/badge/License-Apache 2.0-green" />
    </a>
</p>

## 平台简介
轻舟是一套开源的Java后台管理脚手架。

* 采用前后端分离的模式，微服务架构。
* 具备与若依相同的后台管理功能。
* 后端技术栈：Solon、Solon Cloud、HikariCP、Mybatis-Flex、Redis、Sa-Token、Hutool等。
* 前端技术栈：Vue、Vue-Router、Vuex/Pinia、Element-UI/Element-Plus、Axios等。
* 单体版请移步 [QingZhou Boot](https://gitee.com/opensolon/qingzhou-boot)。

## 平台优势
* 仿照若依开发，轻松上手。
* 后端打包体积更小，启动速度更快，运行时内存占用更低，并发更高。

## 框架对比
* 与基于`SpringCloud`开发的框架（若依）对比：  
 *环境相同，以system模块为例*

|    |  轻舟  |  若依  |
|  :--:  |  :--:  |  :--:  |
|  打包体积（MB）  |  55.30  |  90.88  |
|  启动速度（s）  |  2.3  |  6.3  |
|  内存占用（%） |  6.0  |  11.7  |

## 版本分支
* 默认`master`分支，后端采用`Maven`作为构建工具。
* `gradle`分支，后端采用`Gradle`作为构建工具。
> 不建议直接`clone`分支代码，推荐下载`发行版`或`标签`中的最新版本。

## 系统模块
~~~
com.qingzhou
├── qingzhou-api                          // 接口模块
│       └── qingzhou-api-system           // 系统接口
├── qingzhou-auth                         // 认证中心 [8101]
├── qingzhou-cloud-ui                     // 前端框架（vue2版） [80]
├── qingzhou-cloud-ui-vue3                // 前端框架（vue3版） [80]
├── qingzhou-common                       // 通用模块
│       └── qingzhou-common-core          // 核心模块
│       └── qingzhou-common-datasource    // 多数据源
│       └── qingzhou-common-log           // 日志记录
│       └── qingzhou-common-redis         // 缓存服务
│       └── qingzhou-common-security      // 安全模块
│       └── qingzhou-common-swagger       // 文档模块
├── qingzhou-modules                      // 业务模块
│       └── qingzhou-modules-demo         // 演示服务 [8103]
│       └── qingzhou-modules-docs         // 文档服务 [8095]
│       └── qingzhou-modules-file         // 文件服务 [8091]
│       └── qingzhou-modules-gen          // 代码生成 [8092]
│       └── qingzhou-modules-schedule     // 任务调度 [8093]
│       └── qingzhou-modules-system       // 系统服务 [8102]
├── qingzhou-visual                       // 图形化管理模块
│       └── qingzhou-visual-monitor       // 监控中心 [8094]
├── pom.xml                               // 公共依赖
~~~

## 内置功能
1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线构建：拖动表单元素生成相应的HTML代码。
12. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载。
13. 定时任务：任务调度。
14. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
15. 系统接口：根据业务代码自动生成相关的api接口文档。
16. 在线用户：当前系统中活跃用户状态监控。

## 版本要求
|    |  要求  |  推荐  |
|  :--:  |  :--:  |  :--:  |
|  JDK  |  >= 1.8  |  1.8  |
|  Maven  |  >= 3.6.3  |  3.6.3  |
|  Node |  >= 14  |  14.21.3  |
|  Nacos |  >= 2.0  |  2.0.2  |
|  MySQL |  >= 5.6  |  5.6.36  |
|  Redis |  >= 3  |  3.2.100  |

## 在线体验
演示地址：[https://qing-zhou.cn](https://qing-zhou.cn)\
账密：admin/Admin@123

在线文档：[https://doc.qing-zhou.cn](https://doc.qing-zhou.cn)

## 捐赠支持
<img src="https://gitee.com/opensolon/qingzhou-cloud/raw/master/qingzhou-cloud-ui/src/assets/images/pay.jpg" alt="" />
